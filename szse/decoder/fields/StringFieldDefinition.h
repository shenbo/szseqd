#ifndef BB_SERVERS_SHSE_QD_DECODER_FIELDS_STRINGFIELDDEFINITION_H
#define BB_SERVERS_SHSE_QD_DECODER_FIELDS_STRINGFIELDDEFINITION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */



#include "FieldDefinition.h"
#include "../values/CValues.h"


/**
 * The StringFieldDefinition class represents a FieldDefinition that holds
 * a string value.
 */
class StringFieldDefinition : public FieldDefinition {
  private:
    CString * _value;

  public:
    StringFieldDefinition(const int id, const int type, const int op,
      const int tid, const int s, const bool optional)
        :FieldDefinition(id, type, op, tid, s, optional), _value(new CString()) {}

    /**
     * Deletes the internal allocated value.
     */
    virtual ~StringFieldDefinition() {
      if (_value) delete _value;
      _value = 0;
    }


    virtual void decode(const int slot, Codec & codec, FIX::FieldMap& output);
    virtual void set_current_value(Codec & codec, const string & data);
    virtual string get_value();
    virtual CData * get_data();
    virtual void reset_data();
    virtual ostream & insert(ostream & out) const;
};


#endif // BB_SERVERS_SHSE_QD_DECODER_FIELDS_STRINGFIELDDEFINITION_H
