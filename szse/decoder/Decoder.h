#ifndef BB_SERVERS_SHSE_QD_DECODER_DECODER_H
#define BB_SERVERS_SHSE_QD_DECODER_DECODER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <list>

#include <quickfix/Message.h>

using std::map;
using std::string;

class Codec;


/**
 * This class provides a base for all client C++ decoders.
 */
class Decoder {
  public:
    static bool LOGME;

  protected:
    Codec * codec;
    map<string, Codec *> templates;

  public:
    Decoder();
    virtual ~Decoder();

    size_t decode( std::vector<FIX::Message>& output ) { return decode_message( output ); }
    size_t decode_message( std::vector<FIX::Message>& output );
};


#endif // BB_SERVERS_SHSE_QD_DECODER_DECODER_H
