#ifndef BB_SERVERS_SZSE_QD_STEP_H
#define BB_SERVERS_SZSE_QD_STEP_H

#include <stdint.h>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/function.hpp>

#include <bb/core/Msg.h>
#include <bb/core/string_ref.h>
#include <bb/core/side.h>
#include <bb/core/protobuf/ProtoBufMsg.h>
#include <bb/core/hash_map.h>

#include <bb/io/ByteSink.h>
#include <bb/io/FilterTransport.h>
#include <bb/io/FilterTransportSubscriptionMgr.h>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>

#include "decoder/Codec.h"
#include "decoder/TemplateBuilder.h"
#include "decoder/TransferEncoder.h"

namespace bb {
namespace step {

#define LOG_VERB3 if( unlikely(bb::step::gsettings::verbose_mode >= 3) ) LOG_INFO
#define LOG_VERB10 if( unlikely(bb::step::gsettings::verbose_mode >= 10) ) LOG_INFO
struct gsettings {
    static int verbose_mode;
};

const uint8_t SOH = 0x01;

enum Tag
{
    BEGIN_STRING = 8,
    BODY_LENGTH = 9,
    MSG_TYPE = 35,
    SENDER_COMP_ID = 49,
    TARGET_COMP_ID = 56,
    MSG_SEQ_NUM = 34,
    ENCRYPT_METHOD = 98,
    HEART_BT_INT = 108,
    RAW_DATA = 96,
    CHECK_SUM = 10,
};

struct TagValue
{
    TagValue(Tag, const std::string &);
    Tag tag;
    std::string value;
};

enum ImageStatus
{
    FULL_SNAPSHOT = 1,
    DELTA_SNAPSHOT = 2
};

struct Buffer
{
    Buffer(Buffer *previous, const char *d, size_t l);
    const char *data;
    size_t length;
    Buffer *next;
};

class Message
{
public:
    typedef boost::multi_index::multi_index_container<
                TagValue,
                boost::multi_index::indexed_by<
                    boost::multi_index::sequenced<>,
                    boost::multi_index::ordered_unique< boost::multi_index::member<TagValue, Tag, &TagValue::tag> >
                >
            > TagMap;

    Message(bool isLogon = true);
    ~Message();

    void add(Tag tag, const std::string &value);
    std::string get(Tag tag) const;
    void update(Tag tag, const std::string &value);

    void write(bb::ByteSinkPtr sink);

private:
    TagMap m_tagValue;
};

class FASTDecoder
    : public Decoder
    , boost::noncopyable
{
public:
    FASTDecoder();
    void create_templates( const std::string& template_file );
    size_t decode_buffer( CBytes::byte* buffer, size_t size, std::vector<FIX::Message>& output );

private:
    static TemplateBuilder s_templateBuilder;
    TransferEncoder& m_encoder;
};

class SZSEParser
{
public:
    SZSEParser( std::string& configFileName );

    void decodeSTEP( unsigned char *buf, size_t len );
    void decodeFAST( std::string& tag35MsgType, std::string& tag52SendingTime, unsigned char *buf, size_t len );
    void decodeMarketSnap(const FIX::Message& fix_msg);
    void decodeMarketTbt(const FIX::Message& fix_msg);
    void decodeTickTbt(const FIX::Message& fix_msg);

    timeval_t parse_time_HHMMSSmm(const date_t& todayDate, const std::string& field ) const;

    template<typename T>
    T convert_field( const std::string& field, T error_value, bool log_warn = true ) const;

private:
    FASTDecoder m_decoder;
    boost::unordered_set<std::string> m_ignore_tags;

    std::vector<FIX::Message> m_messages_waiting;
    vector<QuoteSubscriptionMgrPtr> m_subscriptionMgrs;

    // szse proto messages relation
    bb::ProtoBufMsg<acr::SzseMarketSnap>  m_szseMarketSnapCommon;
    bb::ProtoBufMsg<acr::SzseMarketSnap>  m_szseMarketSnapBid;
    bb::ProtoBufMsg<acr::SzseMarketSnap>  m_szseMarketSnapAsk;

    bb::ProtoBufMsg<acr::SzseMarketTbt>  m_szseMarketTbt;
    bb::ProtoBufMsg<acr::SzseTickTbt>  m_szseTickTbt;
};

}
}

#endif // BB_SERVERS_SZSE_QD_STEP_H
