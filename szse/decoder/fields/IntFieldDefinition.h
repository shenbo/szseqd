#ifndef BB_SERVERS_SHSE_QD_DECODER_FIELDS_INTFIELDDEFINITION_H
#define BB_SERVERS_SHSE_QD_DECODER_FIELDS_INTFIELDDEFINITION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */



#include "FieldDefinition.h"
#include "../values/CValues.h"


/**
 * The IntFieldDefinition class is a base class for all integral field definition types.
 */
template<typename type>
class IntFieldDefinition : public FieldDefinition {
  protected:
    type * _value;

  public:
    IntFieldDefinition(const int id, const int _type,
      const int op, const int tid, const int s, const bool optional);

    virtual ~IntFieldDefinition() {
      if (_value) delete _value;
      _value = 0;
    }


    /**
     * Decodes a value.
     *
     * @param slot the position of the field definition in the list of definitions.
     * @param codec the Codec to use to decode the value.
     */
    virtual void decode(const int slot, Codec & codec, FIX::FieldMap& output);
    virtual string get_value();
    virtual CData * get_data();
    virtual void reset_data();
    virtual ostream & insert(ostream & out) const;
};


/**
 * The I32FieldDefinition is a field definition for 32 bit signed integer values.
 */
struct I32FieldDefinition : public IntFieldDefinition<CInt32> {
  I32FieldDefinition(const int id, const int type, const int op, const int tid, const int s, const bool optional);
  virtual void set_current_value(Codec & codec, const string & data);
  CInt32::type get_int() const { return _value -> get_value(); }
};


/**
 * The U32FieldDefinition is a field definition for 32 bit unsigned integer values.
 */
struct U32FieldDefinition : public IntFieldDefinition<CuInt32> {
  U32FieldDefinition(const int id, const int type, const int op, const int tid, const int s, const bool optional);
  virtual void set_current_value(Codec & codec, const string & data);
  CuInt32::type get_int() const { return _value -> get_value(); }
};


/**
 * The I64FieldDefinition is a field definition for 64 bit signed integer values.
 */
struct I64FieldDefinition : public IntFieldDefinition<CInt64> {
  I64FieldDefinition(const int id, const int t, const int op, const int tid, const int s, const bool optional);
  virtual void set_current_value(Codec & codec, const string & data);
  CInt64::type get_int() const { return _value -> get_value(); }
};


/**
 * The U64FieldDefinition is a field definition for 64 bit unsigned integer values.
 */
struct U64FieldDefinition : public IntFieldDefinition<CuInt64> {
  U64FieldDefinition(const int id, const int type, const int op, const int tid, const int s, const bool optional);
  virtual void set_current_value(Codec & codec, const string & data);
  CuInt64::type get_int() const { return _value -> get_value(); }
};


#endif // BB_SERVERS_SHSE_QD_DECODER_FIELDS_INTFIELDDEFINITION_H
