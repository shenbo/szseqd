#include <cstdlib>
#include <iostream>
#include <errno.h>
#include <sys/socket.h>

#include <boost/foreach.hpp>
#include <boost/program_options.hpp>

#include <bb/core/env.h>
#include <bb/core/network.h>
#include <bb/core/DatedFilenameFormatter.h>
#include <bb/core/PersistentSequenceNumber.h>
#include <bb/core/SelectDispatcher.h>
#include <bb/core/UnixSigImpl.h>
#include <bb/core/Log.h>
#include <bb/io/Socket.h>
#include <bb/io/MsgBuf.h>

#include "step.h"

class VdeClient
{
public:
    VdeClient(bb::SelectDispatcher &sd, const bb::sockaddr_ipv4_t &vdeAddr, std::string& configFilename );

    void run();

    void try2ReportStatus(); 
    void tryConnect();

private:
    void handle_connect();
    void handle_read();

    void print_received_data(unsigned char *buf_begin, unsigned char *buf_end);
    char* generate_checksum( unsigned char *buf, size_t bufLen );

    bb::SelectDispatcher &m_dispatcher;
    bb::sockaddr_ipv4_t m_vdeAddr;
    bb::PersistentSequenceNumber m_seq;
    bb::TCPSocketPtr m_sock;
    bb::Subscription m_connectSub;
    bb::Subscription m_readSub;

    static const char *FIX_MARKER;
    static const size_t FIX_MARKER_LEN;
    static const size_t FIX_CHECKSUM_TAG_LEN;
    static const unsigned MAX_MSG_LEN  = 1024 * 1024;

    unsigned char *m_recvp;
    unsigned char *m_begin_unprocessed;
    unsigned char m_bufp[MAX_MSG_LEN+1];

    bb::step::SZSEParser m_step_parser;
    std::string m_sender_comp_id;
    std::string m_target_comp_id;

    time_t m_lastReportTimeStamp;
};

const char *VdeClient::FIX_MARKER = "8=FIXT.1.1";
const size_t VdeClient::FIX_MARKER_LEN = strlen( FIX_MARKER );
const size_t VdeClient::FIX_CHECKSUM_TAG_LEN = strlen( "10=???|" );

void VdeClient::try2ReportStatus() {
    time_t now = time(NULL);
    if ((now - m_lastReportTimeStamp) > 50) {
        bb::step::Message report(false);
        report.write( m_sock );

        m_lastReportTimeStamp = now;
    }
}

void VdeClient::tryConnect() {
    m_sock->setNonBlocking();

    if( m_sock->connect( m_vdeAddr ) )
    {
        m_dispatcher.createReadCB( m_readSub, m_sock, boost::bind( &VdeClient::handle_read, this ) );
    }
    else
    {
        if( errno == EINPROGRESS || errno == EAGAIN )
        {
            m_dispatcher.createWriteCB(m_connectSub, m_sock, boost::bind( &VdeClient::handle_connect, this ) );
        }
        else
        {
            LOG_ERROR << "reconnected but failed to connect to " << m_vdeAddr << std::endl;
        }
    }
}

VdeClient::VdeClient( bb::SelectDispatcher &sd, const bb::sockaddr_ipv4_t &vdeAddr, std::string& configFilename )
    : m_dispatcher(sd),
      m_vdeAddr(vdeAddr),
      m_seq(bb::DatedFilenameFormatter("vss", bb::date_t::today(), vdeAddr.toString() + ".seq"), false),
      m_sock(boost::make_shared<bb::TCPSocket>()),
      m_recvp( m_bufp ),
      m_begin_unprocessed( m_bufp ),
      m_step_parser( configFilename )
{
    bb::LuaState& luaState = bb::DefaultCoreContext::getEnvironment()->luaState();
    luaState.load( configFilename.c_str() );

    try
    {
        m_sender_comp_id = luabind::object_cast<std::string>( luaState.root()["sender_comp_id"] );
        m_target_comp_id = luabind::object_cast<std::string>( luaState.root()["target_comp_id"] );
    }
    catch( luabind::cast_failed& ex )
    {
        LOG_ERROR << "Entry missing or invalid in " << configFilename << bb::endl;
        exit( EXIT_FAILURE );
    }

    tryConnect();

    m_lastReportTimeStamp = time(NULL);
}

void VdeClient::handle_connect()
{
    int sock_error;
    socklen_t sock_error_len = sizeof( sock_error );
    if( ::getsockopt(m_sock->getFD(), SOL_SOCKET, SO_ERROR, &sock_error, &sock_error_len) == 0 )
    {
        if( sock_error == 0 )
        {
            LOG_INFO << "successfully connected to server" << std::endl;;
            m_dispatcher.createReadCB( m_readSub, m_sock, boost::bind( &VdeClient::handle_read, this ) );
            bb::step::Message logon;
            logon.add(bb::step::Tag(34), "1");
            logon.add(bb::step::Tag(49), m_sender_comp_id);
            logon.add(bb::step::Tag(52), string("20161125-10:13:33.222")); // hard code could also login well
            logon.add(bb::step::Tag(56), m_target_comp_id);
            logon.add(bb::step::Tag(98), "0");
            logon.add(bb::step::Tag(108), "1000");
            logon.add(bb::step::Tag(1137), string("9"));
            logon.add(bb::step::Tag(1407), string("124")); 
            logon.add(bb::step::Tag(1408), string("STEP1.20_SZ_1.01")); 
            logon.write( m_sock );
        }
        else
        {
            LOG_ERROR << "failed to connect: " << ::strerror(sock_error) << bb::endl;
        }
    }
    else
    {
        LOG_ERROR << "failed to retrieve socket connection status: " << ::strerror(errno) << bb::endl;
    }

    m_connectSub.reset();
}

void VdeClient::handle_read()
{
    try
    {
        size_t nread = m_sock->recv( m_recvp, MAX_MSG_LEN - (m_recvp - m_bufp) );
        unsigned char *end_of_stream = m_recvp + nread - 1;

        //mark the end of the stream, make string process safer
        //sizeof(m_bufp) is MAX_MSG_LEN+1, this mark operation will not exceed the buffer boundary
        *(end_of_stream+1) = '\0';

        LOG_VERB3 << "Received " << nread << " bytes. Buffer capacity at " << std::setprecision(2) << std::fixed
                << 100.0 * (end_of_stream - m_bufp) / MAX_MSG_LEN << "%" << bb::endl;

        // if running on a very special mode...
        if( bb::step::gsettings::verbose_mode >= 15 )
            print_received_data( m_bufp, end_of_stream );

        bool fix_whole_msg_received = false;
        unsigned char* begin_fix_msg;
        unsigned char* end_fix_msg;

        // process all valid received FIX messages
        do
        {
            fix_whole_msg_received = false;

            // check if we have the minimum amount of data for a FIX header and FIX message size info
            if( end_of_stream - m_begin_unprocessed < 25 )
                break;

            //check FIX marker and get expected FIX message length
            if( memcmp( m_begin_unprocessed, FIX_MARKER, FIX_MARKER_LEN ) == 0 )
            {
                begin_fix_msg = m_begin_unprocessed;
                LOG_VERB10 << "FIX marker found at " << (begin_fix_msg - m_bufp) << " offset bytes from buf" << bb::endl;

                const char* const bodylen_str_start = (char*)begin_fix_msg + FIX_MARKER_LEN + 3 ;
                const char* const bodylen_str_end_next_SOH = strchr(bodylen_str_start, bb::step::SOH);

                BB_THROW_EXASSERT( (bodylen_str_end_next_SOH!=NULL), "(bodylen_str_next_SOH!=NULL)" );
                BB_THROW_EXASSERT( (bodylen_str_end_next_SOH-bodylen_str_start) > 1,
                                   "(bodylen_str_next_SOH-bodylen_str_start) > 1" );

                const int body_len = bb::fixedstr2num<int, 10>( bodylen_str_start, bodylen_str_end_next_SOH );

                end_fix_msg = (unsigned char*)bodylen_str_end_next_SOH + body_len + FIX_CHECKSUM_TAG_LEN;
                fix_whole_msg_received = end_fix_msg <= end_of_stream;

                LOG_VERB10 << "Body len: " <<  body_len << bb::endl;
                LOG_VERB10 << "All message has been received: " << fix_whole_msg_received << bb::endl;

                if( fix_whole_msg_received && bb::step::gsettings::verbose_mode >= 3 )
                {
                    unsigned char *fix_cksum = end_fix_msg - 3;
                    char* calc_cksum = generate_checksum( begin_fix_msg, end_fix_msg - begin_fix_msg - 6 );
                    if( memcmp( fix_cksum, calc_cksum, 3 ) != 0 )
                    {
                        LOG_ERROR << "Checksum ERROR! Received checksum: " << (char)fix_cksum[0] << (char)fix_cksum[1] << (char)fix_cksum[2]
                                  << " Calculated checksum: " << calc_cksum << bb::endl;
                        print_received_data( begin_fix_msg, end_fix_msg );
                    }
                }
            }
            else
            {
                LOG_ERROR << "Garbage data found! Please check!" << bb::endl;
                print_received_data( m_begin_unprocessed, end_of_stream );

                //try to recover by getting the next start pos
                char* nextFixMarkPos = strstr( (char*)m_begin_unprocessed, FIX_MARKER );
                if( nextFixMarkPos )
                {
                    m_begin_unprocessed = (unsigned char*)nextFixMarkPos;
                    continue;
                }
                else
                {
                    break;
                }
            }

            if( fix_whole_msg_received )
            {
                LOG_VERB10 << "Decoding FIX message" << bb::endl;
                m_begin_unprocessed = end_fix_msg + 1;
                m_step_parser.decodeSTEP( begin_fix_msg, end_fix_msg - begin_fix_msg );
            }
        }
        while( fix_whole_msg_received && m_begin_unprocessed < end_of_stream );

        // if the whole buffer has been used up, copy all unprocessed data to the beginning and read more from network
        if( end_of_stream == m_bufp + MAX_MSG_LEN - 1 )
        {
            if( m_begin_unprocessed == m_bufp )
            {
                LOG_ERROR << "Buffer is not big enough to process a single message, increase buffer size! Unrecoverable error, exiting..." << bb::endl;
                exit( EXIT_FAILURE );
            }
            else
            {
                size_t unprocessed_len = end_of_stream - m_begin_unprocessed + 1;
                LOG_VERB10 << "End of buffer reached. Copying " << unprocessed_len << " unprocessed bytes to beginning of buffer..." << bb::endl;
                memmove( m_bufp, m_begin_unprocessed, unprocessed_len );
                m_begin_unprocessed = m_bufp;
                m_recvp = m_bufp + unprocessed_len;
            }
        }
        else if( m_begin_unprocessed == end_of_stream + 1 )
        {
            //all data in the buffer are consumed
            LOG_VERB10 << "Receive buffer pointer reset" << bb::endl;
            m_begin_unprocessed = m_recvp = m_bufp;
        }
        else
        {
            m_recvp = end_of_stream + 1;
        }
    }
    catch (const bb::Error &ex)
    {
        LOG_ERROR << "handle_read error: " << ex.what() << std::endl;
        m_readSub.reset();
        
        if(errno != 0) { // reconnect while any exceptions in socket level
            LOG_ERROR << "unexpected exception catched, socket errno=" << errno << ", try to connect again." << std::endl;

            sleep(1);

            m_sock.reset();
            m_sock = boost::make_shared<bb::TCPSocket>();

            tryConnect();
        }
    }
}

char* VdeClient::generate_checksum( unsigned char *buf, size_t bufLen )
{
    size_t idx;
    unsigned int cks;
    for( idx = 0, cks = 0; idx < bufLen; cks += (unsigned int)buf[ idx++ ] );

    static char tmpBuf[ 4 ];
    sprintf( tmpBuf, "%03d", (unsigned int)( cks % 256 ) );
    return tmpBuf;
}

void VdeClient::print_received_data(unsigned char *buf_begin, unsigned char *buf_end)
{
    for (size_t i = 0; buf_begin + i < buf_end; ++i)
    {
        if (buf_begin[i] == bb::step::SOH)
            std::cout << "|";
        else
            std::cout << buf_begin[i];
    }
    std::cout << std::endl;
}


int main(int argc, char* argv[])
{
    std::vector<std::string> vde;
    std::string configFilename;
    int verbosity;
    std::string wdir;
    std::string pidfile;

    namespace po = boost::program_options;
    po::options_description visible_options;
    visible_options.add_options()
        ("help",           "produce help message")
        ("vde",            po::value<std::vector<std::string> >(&vde), "host:port of the VDE (required)")
        ("config-file,c",  po::value( &configFilename )->default_value( "" ), "Lua config for SRC_SZSE filter transport" )
        ("verbose,v",      po::value<int32_t>(&verbosity)->default_value(0), "verbosity")
        ("working-dir,w",  po::value<std::string>(&wdir), "working directory")
        ("pidfile",        po::value<std::string>(&pidfile), "if specified, pid is written to this file.")
        ;
    po::variables_map vm;
    try
    {
        po::store(po::command_line_parser(argc, argv).options(
                    po::options_description().add(visible_options)).run(), vm);
        po::notify(vm);
    }
    catch (const po::error &e)
    {
        LOG_ERROR << e.what() << bb::endl;
        LOG_ERROR << visible_options << bb::endl;
        return 0;
    }

    if (vm.count("help"))
    {
        LOG_ERROR << visible_options << bb::endl;
        return 0;
    }


    if (vm.count("working-dir")) {
        if (chdir(wdir.c_str())) {
            LOG_PANIC << "Failed to change directory to " << wdir << ", exiting." << bb::endl;
            return -1;
        } else
        LOG_INFO << "Working directory set to: " << wdir << bb::endl;
    }

    if (vm.count("pidfile")) {
        pid_t pid = getpid();
        FILE* fp = fopen(pidfile.c_str(), "w");
        fprintf(fp, "%d", pid);
        fclose(fp);
    }


    bb::step::gsettings::verbose_mode = verbosity;

    try
    {
        bb::SelectDispatcher sd;
        sd.sigAction(SIGINT, bb::SignalException::Throw());
        sd.sigAction(SIGTERM, bb::SignalException::Throw());

        typedef boost::scoped_ptr<VdeClient> VdeClientSptr;
        VdeClientSptr *clients = new VdeClientSptr[ vde.size() ];
        size_t i = 0;
        BOOST_FOREACH(const std::string &vdeAddr, vde)
        {
            size_t colon_pos = vdeAddr.rfind( ':' );
            if( colon_pos != std::string::npos )
            {
                std::string host = vdeAddr.substr( 0, colon_pos );
                int port = boost::lexical_cast<int>( vdeAddr.substr(colon_pos + 1) );
                bb::sockaddr_ipv4_t sockaddr = bb::lookupaddr_blocking( host, port );
                clients[ i++ ].reset( new VdeClient( sd, sockaddr, configFilename ) );
            }
            else
            {
                LOG_WARN << "malformed vde address: " << vdeAddr << bb::endl;
            }
        }

        while (true)
        {
            try
            {
                sd.select();
            }
            catch (const bb::BadPacketError& e)
            {
                LOG_WARN << e.what() << bb::endl;
            }

            // client try to report
            for (size_t i = 0; i < vde.size(); i++) {
                clients[i]->try2ReportStatus();
            }
        }
    }
    catch (bb::SignalException& ex)
    {
        LOG_ERROR << "got signal " << ex.sigName() << " ... exiting." << bb::endl;
    }

    return 0;
}

