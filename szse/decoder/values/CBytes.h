#ifndef BB_SERVERS_SHSE_QD_DECODER_VALUES_CBYTES_H
#define BB_SERVERS_SHSE_QD_DECODER_VALUES_CBYTES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iostream>

#include "CValue.h"
#include "../Utils.h"

using std::ostream;
using std::cout;
using std::endl;

typedef uint8_t CBytesType;
class CBytes : public CValue<CBytesType *> {
  public:
    typedef CBytesType byte;

  private:
    unsigned int _capacity;

    void clear() {
      if (_value != 0) delete [] _value;
      _value = 0;
      _length = -1;
      _capacity = 0;
      _null = true;
    }

  public:
    CBytes(const int capacity) {
      _value = 0;
      set_value(new byte[capacity]);
      for (int i = 0; i < capacity; ++i) _value[i] = 0;
      _length = 0;
      _capacity = capacity;
    }

    CBytes(const CBytes & copy) {
      _value = 0;
      _length = copy._length;
      _capacity = copy._capacity;
      set_value(new byte[_length]);
      for (int i = 0; i < _length; ++i) _value[i] = copy._value[i];
    }

    virtual ~CBytes() {
      clear();
    }

    virtual byte * get_value() const {
      // this forces users to set the length after setting the value.
      assert(_value == 0 || _length != -1);
      return _value;
    }

    /**
     * @return the byte indexed by the argument.
     */
    byte get_byte(const int index) const {
      assert(index < _length  && index >= 0);
      return _value[index];
    }

    /**
     * @return true if the argument byte is a stop byte.
     */
    bool set_byte(const int index, const byte & value) {
      // cout << "index=" << index << ", _capacity=" << _capacity << std::endl;
      assert(index < (int) _capacity);
      bool stop = (value >= 0x80);
      _value[index] = (stop ? value & 0x7F : value);
      if (stop) _length = index + 1;
      return stop;
    }

    /**
     * Deletes the old byte array, if present, and replaces it with the argument
     * array.
     */
    virtual void set_value(const type & value) {
      clear();
      _null = value == 0;
      _value = value;
      _capacity = sizeof(value);
    }
};


ostream & operator << (ostream & out, const CBytes & bytes);


#endif // BB_SERVERS_SHSE_QD_DECODER_VALUES_CBYTES_H
