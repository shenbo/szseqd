// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "../FASTDecoder.h"


void ScaledMantissaFieldDefinition::decode(const int slot, Codec & codec, FIX::FieldMap& output) {
  reset_data();
  
  if (Decoder::LOGME) {
    FLogger::get_instance().set_id(Utils::to_string(get_field_id()));
    FLogger::get_instance().set_field(get_field_type_name());
    FLogger::get_instance().set_operator(get_field_operator_name());
    FLogger::get_instance().set_optional(is_optional() ? "true" : "false");
    if (is_mandatory()) {
      FLogger::get_instance().set_pmap("M");
    } else {
      FLogger::get_instance().set_pmap((codec.get_presence_map().get_bit(slot)) ? "1" : "0");
    }
    
    CurrentValues & cvs = codec.get_current_values();
    bool valid = cvs.is_valid(get_slot());
    string currentValue = valid ? Utils::to_string(cvs.get_i64(get_slot())) : "null";
    FLogger::get_instance().set_current(currentValue);
  }
  
  ScaledExponentFieldDefinition * efd = (ScaledExponentFieldDefinition *)
    codec.get_definitions()[slot - 1];
  
  const bool is_present = codec.get_presence_map().get_bit(get_slot());
  const bool is_mandatory = Utils::mandatory_field(get_field_operator(), is_optional());
  const bool is_null = efd -> get_data() -> is_null();
  if (is_null && optional_decimal && (!is_present || (is_present && is_mandatory))) {
    if (Decoder::LOGME) {
      FLogger::get_instance().pretty_print_display();
      FLogger::get_instance().reset();
    }
    return;
    
  } else {
    codec.decode(slot, (CInt64 *) get_data());
    
    CDecimal decimal;
    decimal.set_exponent(((CInt32 *) efd -> get_data()) -> get_value());
    decimal.set_mantissa(((CInt64 *) get_data()) -> get_value());
    
    output.setField( get_field_id(), decimal.get_decimal() );
    return;
  }
}
