#ifndef BB_SERVERS_SHSE_QD_DECODER_UTILS_H
#define BB_SERVERS_SHSE_QD_DECODER_UTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstdio>
#include <sstream>
#include "fields/FieldDefinition.h"

using std::stringstream;


/**
 * This class provides basic functions to various internal classes.
 */
class Utils {
  public:
    /**
     * @return a string equal to the argument input type.
     */
    template<typename type>
    static string to_string(const type & input) {
      stringstream ss;
      ss << input;
      return ss.str();
    }


    /**
     * @return a zero string of length equal to the argument count.
     */
    template<typename type>
    static string zeros(const type & count) {
      char buffer[50];
      assert (count < 50);
      sprintf(buffer, ("%0" + to_string(count) + "d").c_str(), 0);
      return buffer;
    }


    /**
     * @return a hex string for the argument input.
     */
    template<typename type>
    static string to_hex(const type & input) {
      char buffer[20];
      sprintf(buffer, "%x", input);
      return string(buffer);
    }


    /**
     * Convert long values to fit into 32 bits of data
     *
     * @param value the long value
     * @return 32 bit representation of the value inside a 64 bit field.
     */
    static int64_t convertLongUInt(const int64_t & value) {
      const uint64_t unsigned_value = value << 32;
      return unsigned_value >> 32;
    }


    /**
     * Is the field always present or will it's presence be defined
     * within a presence map
     *
     * @param fieldOperator the FieldDefinition.OPERATOR
     * @return is this field always present
     */
    static bool mandatory_field(const int & field_operator, const bool & optional) {
      switch(field_operator) {
        case FieldDefinition::OPERATOR_DELTA:
        case FieldDefinition::OPERATOR_DEFAULT:
          return true;
        case FieldDefinition::OPERATOR_CONSTANT:
          return !optional;
        default:
          return false;
      }
    }

  private:
    Utils() {}
    virtual ~Utils() {}
};


#endif // BB_SERVERS_SHSE_QD_DECODER_UTILS_H
