#ifndef BB_SERVERS_SHSE_QD_DECODER_PRESENCEMAP_H
#define BB_SERVERS_SHSE_QD_DECODER_PRESENCEMAP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/Error.h>

/**
 * A PresenceMap is a bit field that determines which, if any, of the fields
 * in a template are present in the following message bits.  A PresenceMap
 * always comes before the fields it describes.
 */
class PresenceMap {
  private:
    const int _slots;
    bool * bits;
    int _size;

    /**
     * Disabled.
     */
    PresenceMap(const PresenceMap & copy) :_slots(0), bits(0), _size(0) {}

  public:
    PresenceMap(const int slots)
      :_slots(slots), _size(0)
    {
      // TODO: std::vector<bool> will take up less space and will clean up
      // after itself.
      bits = new bool[slots];
    }


    virtual ~PresenceMap() {
      if (bits != 0) delete [] bits;
      bits = 0;
      _size = 0;
    }


    bool * get_bits() const {
      return bits;
    }


    int get_size() const {
      return _size;
    }


    void set_size(const int size) {
      _size = size;
    }


    void reset() {
      while (_size > 0) bits[--_size] = false;
    }


    bool get_bit(const int slot) const {
      BB_THROW_EXASSERT_SSX(slot < _slots, "slot out of range");
      return bits[slot];
    }


    void set_bit(const int slot) {
      BB_THROW_EXASSERT_SSX(slot < _slots, "slot out of range");
      if (slot >= _size) _size = slot + 1;
      bits[slot] = true;
    }
};


#endif // BB_SERVERS_SHSE_QD_DECODER_PRESENCEMAP_H
