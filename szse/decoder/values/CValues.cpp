// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "../FASTDecoder.h"


ostream & operator << (ostream & out, const CInt32 & i32) {
  return i32.insert(out);
}


ostream & operator << (ostream & out, const CuInt32 & u32) {
  return u32.insert(out);
}


ostream & operator << (ostream & out, const CInt64 & i64) {
  return i64.insert(out);
}


ostream & operator << (ostream & out, const CuInt64 & u64) {
  return u64.insert(out);
}


ostream & operator << (ostream & out, const CString & str) {
  return str.insert(out);
}


