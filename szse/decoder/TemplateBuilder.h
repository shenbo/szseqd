#ifndef BB_SERVERS_SHSE_QD_DECODER_TEMPLATEBUILDER_H
#define BB_SERVERS_SHSE_QD_DECODER_TEMPLATEBUILDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <list>
#include <string>

using std::map;
using std::list;
using std::string;

class Codec;
class Element;
class FieldDefinition;
class TransferEncoder;


/**
 * This class creates FieldDefinitions and Codecs using the values stored in
 * Elements loaded from the template.xml file.
 */
class TemplateBuilder {
  public:
    typedef list<Codec *> Codecs;
    typedef map<string, Codec *> TemplateMap;
    typedef TransferEncoder * Encoder;
    typedef FieldDefinition * Definition;
    typedef list<Definition> Instances;
    typedef map<Definition, string> DefinitionMap;

    static const string FAST_ID, FAST_PRESENCE, FAST_OPTIONAL, FAST_VALUE;

  private:
    Element * root;
    Instances instances;
    Codecs codecs;

  public:
    TemplateBuilder() :root(0) {}
    virtual ~TemplateBuilder();


    /**
     * Creates a Codec using the defined fields in the argument filename.
     *
     * @return the new Codec.
     */
    Codec * create_templates(const string & filename,
      TemplateMap & templates, const Encoder & encoder);


    /**
     * Creates a Codec using the argument filenames.
     *
     * @returns the new Codec.
     */
    Codec * create_templates(vector<string> & filenames,
      TemplateMap & templates, const Encoder & encoder);


    /**
     * Creates a new Codec using the argument filename.
     *
     * @return the new Codec.
     */
    Codec * parse_template(const string & filename,
      TemplateMap & templates, const Encoder & encoder);


    /**
     * Creates a new Codec using the data contained in the argument root Element.
     *
     * @return the new Codec.
     */
    Codec * parse_template_xml(Element * root,
      TemplateMap & templates, const Encoder & encoder);


    /**
     * Populates the argument DefinitionMap using the values contained in the
     * argument root Element.
     */
    void parse_template(DefinitionMap & definitions, const int template_id,
      Element * root, TransferEncoder * encoder, const int start_count);

  private:

    /**
     * Helper method.
     *
     * Adds a FieldDefinition for the current decimal field definition.
     *
     * @return 1 if the added FieldDefinition was for a scaled decimal
     *           definition; otherwise returns 0.
     */
    int add_decimal_field_definition(DefinitionMap & definitions, const int id,
      const int slot, const int template_id, Element * decimal_element);


    /**
     * Helper method.
     *
     * @return true if the argument Element is flagged as optional.
     */
    bool is_optional(Element * element);


    /**
     * Helper method.
     *
     * @return the operator child Element for the argument element.
     */
    Element * get_operator_element(Element * element);


    /**
     * Helper method.
     *
     * @return the default value for the argument Element.
     */
    string get_default_value(Element * element);


    /**
     * Helper method.
     *
     * @return the int operator type code for the argument element.
     */
    int get_operator_type(Element * operator_element, const bool optional,
      const bool field_optional);

  private:
    TemplateBuilder(const TemplateBuilder & copy) {}
};

#endif // BB_SERVERS_SHSE_QD_DECODER_TEMPLATEBUILDER_H
