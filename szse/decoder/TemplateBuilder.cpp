// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "FASTDecoder.h"

const string TemplateBuilder::FAST_ID = "id";
const string TemplateBuilder::FAST_PRESENCE = "presence";
const string TemplateBuilder::FAST_OPTIONAL = "optional";
const string TemplateBuilder::FAST_VALUE = "value";


I32FieldDefinition * i32(int id, int OPERATOR, int template_id, int slot, bool optional) {
  return new I32FieldDefinition(id, FieldDefinition::TYPE_I32, OPERATOR, template_id, slot, optional);
}

I64FieldDefinition * i64(int id, int OPERATOR, int template_id, int slot, bool optional) {
  return new I64FieldDefinition(id, FieldDefinition::TYPE_I64, OPERATOR, template_id, slot, optional);
}

U32FieldDefinition * u32(int id, int OPERATOR, int template_id, int slot, bool optional) {
  return new U32FieldDefinition(id, FieldDefinition::TYPE_U32, OPERATOR, template_id, slot, optional);
}

U64FieldDefinition * u64(int id, int OPERATOR, int template_id, int slot, bool optional) {
  return new U64FieldDefinition(id, FieldDefinition::TYPE_U64, OPERATOR, template_id, slot, optional);
}


TemplateBuilder::~TemplateBuilder() {
  if (root != 0) delete root;
  root = 0;
  for (Instances::iterator i = instances.begin(), e = instances.end(); i != e; ++i) {
    if (*i) delete *i;
    *i = 0;
  }
  instances.clear();
  for (Codecs::iterator i = codecs.begin(), e = codecs.end(); i != e; ++i) {
    if (*i) delete *i;
    *i = 0;
  }
  codecs.clear();
}


Codec * TemplateBuilder::create_templates(const string & filename, 
  TemplateMap & templates, const Encoder & encoder)
{
  return parse_template(filename, templates, encoder);
}


Codec * TemplateBuilder::create_templates(vector<string> & filenames, 
  TemplateMap & templates, const Encoder & encoder)
{
  Codec * first = 0;
  for (vector<string>::iterator i = filenames.begin(), e = filenames.end(); i != e; ++i) {
    Codec * codec = parse_template(*i, templates, encoder);
    if (first == 0) first = codec;
  }
  return first;
}


string to_lower(string input) {
  for (unsigned int i = input.length(); i; --i) input[i - 1] = tolower(input[i - 1]);
  return input;
}


Codec * TemplateBuilder::parse_template(const string & filename, 
  TemplateMap & templates, const Encoder & encoder)
{
  if (root != 0) delete root;
  root = Element::get_root(filename);
  
  if (to_lower(root -> get_name()) == "templates") {
    Codec * first = 0;
    for (Element::Iterator i = root -> children_begin(), e = root -> children_end(); i != e; ++i) {
      Codec * codec = parse_template_xml(*i, templates, encoder);
      if (first == 0) first = codec;
    }
    return first;
  }
  return parse_template_xml(root, templates, encoder);
}


Codec * TemplateBuilder::parse_template_xml(Element * root, 
  TemplateMap & templates, const Encoder & encoder)
{
  int template_id = atoi(root -> attribute(FAST_ID).c_str());
  DefinitionMap definitions;
 
  instances.push_back(u32(0, FieldDefinition::OPERATOR_COPY, template_id, 0, false));
  definitions[instances.back()] = "";
  parse_template(definitions, template_id, root, encoder, 1);
  codecs.push_back(new Codec(*encoder, definitions, template_id));

  return templates[root -> attribute(FAST_ID)] = codecs.back();
}


void TemplateBuilder::parse_template(DefinitionMap & definitions, 
  const int template_id, Element * root, TransferEncoder * encoder, 
  const int start_count)
{
  Element::Iterator i = root -> children_begin(), e = root -> children_end();
  for (int count = start_count; i != e; ++i) {
    Element * element = *i;
    string name = to_lower(element -> get_name());
    if (name == "typeref") continue;

    Element * operator_element = get_operator_element(element);

    bool optional = is_optional(element);
    int operator_type = get_operator_type(operator_element, optional, optional);

    string default_value = get_default_value(operator_element);
    string id_str = element -> attribute(FAST_ID);

    const int id = (id_str == "") ? -1 : atoi(id_str.c_str());
    cout << "***** template_id=" << template_id
         << ", id=" << id
         << ", operator_type=" << operator_type
         << std::endl;
    
    if (name == "uint32" || name == "length") {
      assert(CInt64::parse(default_value) >= 0);
      instances.push_back(u32(id, operator_type, template_id, count, optional));
    } else if (name == "int32") {
      instances.push_back(i32(id, operator_type, template_id, count, optional));
    } else if (name == "uint64") {
      assert(CInt64::parse(default_value) >= 0);
      instances.push_back(u64(id, operator_type, template_id, count, optional));
    } else if (name == "int64") {
      instances.push_back(i64(id, operator_type, template_id, count, optional));
    } else if (name == "string") {
      instances.push_back(new StringFieldDefinition(id, 
        FieldDefinition::TYPE_STRING, operator_type, template_id, count, optional));
    } else if (name == "decimal") {
      count += 1 + add_decimal_field_definition(definitions, id, count, template_id, element);
      continue;
    } else if (name == "sequence") {

      default_value = get_default_value(
        get_operator_element(
          *(
            ++(
              element -> children_begin()
            )
          )
        )
      );
      
      DefinitionMap group_definitions;
      parse_template(group_definitions, template_id, element, encoder, 0);
      codecs.push_back(new Codec(*encoder, group_definitions, -1));
      instances.push_back(new GroupFieldDefinition(-1, -1, FieldDefinition::OPERATOR_DEFAULT, 
        template_id, count, *codecs.back(), is_optional(element)));
      default_value = "";
    } else continue; // XML comment
    definitions[instances.back()] = default_value;
    ++count;
  }

  if (template_id == 3004) {
      // exit(1);
  }
}


int TemplateBuilder::add_decimal_field_definition(DefinitionMap & definitions,
  const int id, const int slot, const int template_id, Element * decimal_element)
{
  I32FieldDefinition * exponent = i32(id, FieldDefinition::OPERATOR_DEFAULT, template_id, slot, false);
  I64FieldDefinition * mantissa = i64(id, FieldDefinition::OPERATOR_DEFAULT, template_id, slot, false);

  int decimal_operator_type = FieldDefinition::OPERATOR_DEFAULT;
  
  string decimal_dv = "";
  string mantissa_dv = "";
  string exponent_dv = "";
  bool optional = is_optional(decimal_element);
  bool scaled = false;
  
  for (Element::Iterator i = decimal_element -> children_begin(), e = decimal_element -> children_end(); i != e; ++i) 
  {
    Element * element = *i;

    //                      Check operator type                     
    int ot = get_operator_type(element, optional, optional);
    decimal_operator_type = (ot < 0) ? FieldDefinition::OPERATOR_DEFAULT : ot;
    decimal_dv = get_default_value(element);

    string name = to_lower(element -> get_name());
    scaled = scaled || name == "exponent" || name == "mantissa";
    if (name == "exponent") {
      Element * operator_element = get_operator_element(element);
      
      // Bank definition <exponent/>
      if (operator_element == 0) continue;
      
      bool exp_optional = is_optional(operator_element);
      exponent_dv = get_default_value(operator_element);
      
      if (exponent != 0) delete exponent;
      exponent = i32(id, get_operator_type(operator_element, exp_optional, optional), 
        template_id, slot, exp_optional);
      continue;
    }
    if (name == "mantissa") {
      Element * operator_element = get_operator_element(element);
      
      // Bank definition <mantissa/>
      if (operator_element == 0) continue;

      bool m_optional = is_optional(operator_element);
      mantissa_dv = get_default_value(operator_element);
      
      if (mantissa != 0) delete mantissa;
      mantissa = i64(id, get_operator_type(operator_element, m_optional, optional), 
        template_id, slot, m_optional);
    }
  }
  instances.push_back(exponent);
  instances.push_back(mantissa);
  
  // Scaled # (uses 2 field)
  if (scaled) {
    instances.push_back(new ScaledExponentFieldDefinition(id, FieldDefinition::TYPE_I32,
      exponent -> get_field_operator(), template_id, slot, 
      exponent -> is_optional() || optional));
    definitions[instances.back()] = exponent_dv;
    
    instances.push_back(new ScaledMantissaFieldDefinition(id, FieldDefinition::TYPE_I64,
      mantissa -> get_field_operator(), template_id, slot + 1, 
      mantissa -> is_optional(), optional));
    definitions[instances.back()] = mantissa_dv;
    
    return 1;
  }
  
  // Default scaled # (uses 1 field)
  exponent -> set_optional(optional);
  
  if (decimal_operator_type == FieldDefinition::OPERATOR_DEFAULT_PRESENCE) {
    exponent -> set_field_operator(FieldDefinition::OPERATOR_DEFAULT_PRESENCE);
  } 
  
  instances.push_back(new ScaledFieldDefinition(id, FieldDefinition::TYPE_SCALED_NUMBER, 
    decimal_operator_type, template_id, slot, exponent, mantissa, optional));
  definitions[instances.back()] = decimal_dv;

  return 0;
}


bool TemplateBuilder::is_optional(Element * element) {
  return to_lower(element -> attribute(FAST_PRESENCE)) == FAST_OPTIONAL;
}


Element * TemplateBuilder::get_operator_element(Element * element) {
  Element::Iterator i = element -> children_begin(), e = element -> children_end();
  return (i == e) ? 0 : *i;
}


string TemplateBuilder::get_default_value(Element * element) {
  return element == 0 ? "" : element -> attribute(FAST_VALUE);
}


int TemplateBuilder::get_operator_type(Element * element, const bool optional, 
  const bool field_optional)
{
  if (element == 0) return FieldDefinition::OPERATOR_DEFAULT;
  string type = to_lower(element -> get_name());
  if (type == "default") {
    assert(optional || field_optional || get_default_value(element) != "");
    return FieldDefinition::OPERATOR_DEFAULT_PRESENCE;
  }
  if (type == "copy")      return FieldDefinition::OPERATOR_COPY;
  if (type == "increment") return FieldDefinition::OPERATOR_INCREMENT;
  if (type == "delta")     return FieldDefinition::OPERATOR_DELTA;
  if (type == "tail")      return FieldDefinition::OPERATOR_TAIL;
  if (type == "constant") {
    assert(get_default_value(element) != "");
    return FieldDefinition::OPERATOR_CONSTANT;
  }
  return -1;
}
