#ifndef BB_SERVERS_SHSE_QD_DECODER_FIELDS_SCALEDFIELDDEFINITION_H
#define BB_SERVERS_SHSE_QD_DECODER_FIELDS_SCALEDFIELDDEFINITION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */



#include "IntFieldDefinition.h"


/**
 * The ScaledFieldDefinition class represents a decimal FieldDefinition that is defined
 * using a string representation instead of separate exponent and mantissa components.
 */
class ScaledFieldDefinition : public FieldDefinition {
  public:
    typedef I32FieldDefinition * Exponent;
    typedef I64FieldDefinition * Mantissa;

  private:
    Exponent _exponent;
    Mantissa _mantissa;
    CDecimal * _value;

  public:
    ScaledFieldDefinition(const int id, const int type, const int op, const int tid, const int s,
      const Exponent & exponent, const Mantissa & mantissa, const bool optional);

    /**
     * Deletes the allocated internal value.
     */
    virtual ~ScaledFieldDefinition() {
      if (_value) delete _value;
      _value = 0;
    }


    Exponent get_exponent() const { return _exponent; }
    Mantissa get_mantissa() const { return _mantissa; }

    void set_exponent(const Exponent & exponent) { _exponent = exponent; }
    void set_mantissa(const Mantissa & mantissa) { _mantissa = mantissa; }

    virtual void set_current_value(Codec & codec, const string & data);
    virtual void decode(const int slot, Codec & codec, FIX::FieldMap& output);
    virtual string get_value();
    virtual CDecimal * get_data();
    virtual void reset_data();

    void set_null_field();
};


#endif // BB_SERVERS_SHSE_QD_DECODER_FIELDS_SCALEDFIELDDEFINITION_H
