// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "FASTDecoder.h"

#include <quickfix/Message.h>

bool Decoder::LOGME = false;

namespace
{
    FIX::Message tmp;
}

Decoder::Decoder() {
  // nothing to do!
}


Decoder::~Decoder() {
  // nothing to do!
}

namespace {

void printNow() {
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    cout << "[TimeStamp] " << (tm.tm_year + 1900) << "-"
         << (tm.tm_mon + 1) << "-"
         << (tm.tm_mday) << "-"
         << (tm.tm_hour) << ":"
         << (tm.tm_min) << ":"
         << (tm.tm_sec)
         << std::endl;
}
};

size_t Decoder::decode_message( std::vector<FIX::Message>& result ) {
    // cout << "[----- DEBUG ---] decode_message 001\n";

    size_t index = 0;

    vector<Codec *> used;
    CBytes * pmap = codec -> get_presence_map_off_stream();

    while (pmap -> get_length() > 0) {
        codec -> parse_pmap(pmap, true, 0, true);

        // Parse the template id
        U32FieldDefinition * template_field = 
            (U32FieldDefinition *) (codec -> get_definitions()[0]);

        template_field -> decode( 0, *codec, tmp );
        string templateId = template_field -> get_value();

        // printNow();
        // cout << "Received templateID: " << templateId << std::endl;
        if (Decoder::LOGME) {
            FLogger::get_instance().append_buffer("Received templateID: ");
            FLogger::get_instance().append_buffer(templateId.c_str());
            FLogger::get_instance().print_buffer();
            FLogger::get_instance().reset_buffer();
        }

        //Switch Templates
        if (codec -> get_template_id() != ((CuInt32 *) template_field -> get_data()) -> get_value()) {

            if( templates.find( templateId ) == templates.end() )
            {
                // std::cout << "Template " << templateId << " not be fetched from existing loaded templates. Aborting decode..." << std::endl;
                return index;
            }
            else
            {
                codec = templates[templateId];
                if (Decoder::LOGME) FLogger::get_instance().reset_buffer();
            }
        }
    
        codec -> parse_pmap(pmap, true, 0, false);

        //Log the presence map & template id
        if (Decoder::LOGME) {
            FLogger::get_instance().print_buffer();
            FLogger::get_instance().reset_buffer();
            FLogger::get_instance().print_header();
            FLogger::get_instance().pretty_print_display();
            FLogger::get_instance().reset();
        }
    
        FIX::Message& current_message = result.at( index++ );
        current_message.clear();

        // set messageType or templateID in header part
        current_message.getHeader().setField(35, templateId);

        vector<FieldDefinition *> & fieldDefinitions = codec -> get_definitions();
        for (unsigned int i=1; i<fieldDefinitions.size(); ++i) {
            FieldDefinition* ffd = fieldDefinitions[i];
            if (Decoder::LOGME) {
            cout << "****** field_id=" << ffd->get_field_id()
                 << ", field_operator=" << ffd->get_field_operator()
                 << ", field_type=" << ffd->get_field_type()
                 << ", templateId=" << ffd->get_template_id()
                 << ", slot=" << ffd->get_slot()
                 << std::endl;
                 }
            if( FIX::Message::isHeaderField( fieldDefinitions[i]->get_field_id() ) )
                fieldDefinitions[i] -> decode( i, *codec, current_message.getHeader() );
            else if( FIX::Message::isTrailerField( fieldDefinitions[i]->get_field_id() ) )
                fieldDefinitions[i] -> decode( i, *codec, current_message.getTrailer() );
            else
                fieldDefinitions[i] -> decode( i, *codec, current_message );
        }

        if (Decoder::LOGME) FLogger::get_instance().println();
    
        used.push_back(codec);
        pmap = codec -> get_presence_map_off_stream();
    }
  
    // clear dictionary after a batch.
    for (vector<Codec *>::iterator i = used.begin(), e = used.end(); i != e; ++i) {
        (*i) -> reset_dictionary();
    }
    used.clear();

    return index;
}
