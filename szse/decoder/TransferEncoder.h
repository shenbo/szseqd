#ifndef BB_SERVERS_SHSE_QD_DECODER_TRANSFERENCODER_H
#define BB_SERVERS_SHSE_QD_DECODER_TRANSFERENCODER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <sstream>
#include "values/CValues.h"
#include "Decoder.h"
#include "FLogger.h"

using std::stringstream;


class TransferEncoder {
  private:
    static TransferEncoder * instance;
    typedef uint8_t byte;

  public:
    static const int
      PARSE_BUFFER_SIZE = 4096,
      SIGN_MASK_UNSIGNED = 0x00,
      SIGN_MASK_SIGNED = 0x40;

    static TransferEncoder & get_instance() {
      if (instance == 0) instance = new TransferEncoder();
      return *instance;
    }

  private:
    CBytes * parse_buffer;
    CBytes::byte * _input, * bytes;
    int input_pointer, input_length;

    TransferEncoder();
    TransferEncoder(const TransferEncoder & copy) {}

    int parse_bytes();

  public:
    virtual ~TransferEncoder();

    void set_input_stream(CBytes::byte * input, const int length) {
      _input = input;
      input_length = length;
    }

    void reset_input_pointer() {
      input_pointer = 0;
    }

    void parse_bytes(CBytes & data, const int size);
    void parse_string(const bool slot, CString & value);

    void parse_int(const bool slot, CInt32 & value, const bool optional);
    void parse_i64(const bool slot, CInt64 & value, const bool optional);
    void parse_int(const bool slot, CuInt32 & value, const bool optional);
    void parse_u64(const bool slot, CuInt64 & value, const bool optional);

    template<class type>
    void parse_int(CValue<type> & value, const type & sign_mask, const bool is_optional);

    template<class type>
    void parse_int(const bool slot, CValue<type> & value, const bool optional);

  private:
    void parse_int(CValue<CInt64::type> & value, const bool optional);
    void parse_int(CValue<CuInt64::type> & value, const bool optional);
    void parse_int(CValue<CInt32::type> & value, const bool optional);
    void parse_int(CValue<CuInt32::type> & value, const bool optional);
};


#endif // BB_SERVERS_SHSE_QD_DECODER_TRANSFERENCODER_H
