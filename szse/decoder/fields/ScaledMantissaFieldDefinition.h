#ifndef BB_SERVERS_SHSE_QD_DECODER_FIELDS_SCALEDMANTISSAFIELDDEFINITION_H
#define BB_SERVERS_SHSE_QD_DECODER_FIELDS_SCALEDMANTISSAFIELDDEFINITION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include "IntFieldDefinition.h"

class ScaledMantissaFieldDefinition : public I64FieldDefinition {
  private:
    bool optional_decimal;

  public:
    ScaledMantissaFieldDefinition(const int id, const int type, const int op,
      const int tid, const int s, const bool optional, const bool optionalDec)
        : I64FieldDefinition(id, type, op, tid, s, optional),
          optional_decimal(optionalDec) {}

    virtual ~ScaledMantissaFieldDefinition() {}

    virtual void decode(const int slot, Codec & codec, FIX::FieldMap& output);
};

#endif // BB_SERVERS_SHSE_QD_DECODER_FIELDS_SCALEDMANTISSAFIELDDEFINITION_H
