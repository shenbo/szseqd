#ifndef BB_SERVERS_SHSE_QD_DECODER_VALUES_CVALUES_H
#define BB_SERVERS_SHSE_QD_DECODER_VALUES_CVALUES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <stdlib.h>
#include <stdint.h>
#include <string>

/* These were added for compatability with Windows */
#ifdef _WIN32
#  ifndef int8_t
#    define int8_t __int8
#  endif
#  ifndef uint8_t
#    define uint8_t unsigned __int8
#  endif
#  ifndef int16_t
#    define int16_t __int16
#  endif
#  ifndef uint16_t
#    define uint16_t unsigned __int16
#  endif
#  ifndef int32_t
#    define int32_t __int32
#  endif
#  ifndef uint32_t
#    define uint32_t unsigned __int32
#  endif
#  ifndef int64_t
#    define int64_t __int64
#  endif
#  ifndef uint64_t
#    define uint64_t unsigned __int64
#  endif
#endif

#include "CValue.h"
#include "CBytes.h"
#include "CDecimal.h"


/**
 * This struct is defined so that all integer-based CValues are initialized to
 * zero upon creation.
 */
template <typename int_type>
struct IntValue : public CValue<int_type> {
  IntValue() {
    CValue<int_type>::_value = 0;
  }

  virtual void reset() {
    CValue<int_type>::_value = 0;
    CValue<int_type>::_null = true;
  }
};


/**
 * This struct represents a signed 32-bit integer value.
 */
struct CInt32 : public IntValue<int32_t> {
  static int32_t parse(const string & value) {
    return atoi(value.c_str());
  }
};


/**
 * This struct represents a signed 64-bit integer value.
 */
struct CInt64 : public IntValue<int64_t> {
  static int64_t parse(const string & value) {
    return strtoll(value.c_str(), NULL, 0);
  }
};


/**
 * This struct represents an unsigned 32-bit integer value.
 */
struct CuInt32 : public IntValue<uint32_t> {
  static uint32_t parse(const string & value) {
    return strtoul(value.c_str(), NULL, 0);
  }
};


/**
 * This struct represents an unsigned 64-bit integer value.
 */
struct CuInt64 : public IntValue<uint64_t> {
  static uint64_t parse(const string & value) {
    return strtoull(value.c_str(), NULL, 0);
  }
};


/**
 * This struct represents a string value.
 */
struct CString : public CValue<std::string> {
  virtual std::string get_value() const {
    return _null ? "" : _value;
  }
};


ostream & operator << (ostream & out, const CInt32 & value);
ostream & operator << (ostream & out, const CuInt32 & value);
ostream & operator << (ostream & out, const CInt64 & value);
ostream & operator << (ostream & out, const CuInt64 & value);
ostream & operator << (ostream & out, const CString & value);


#endif // BB_SERVERS_SHSE_QD_DECODER_VALUES_CVALUES_H
