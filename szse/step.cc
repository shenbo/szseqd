#include <iostream>
#include <sstream>
#include <numeric>

#include <boost/lexical_cast.hpp>
#include <google/protobuf/stubs/common.h>

#include <bb/core/env.h>
#include <bb/core/timeval.h>
#include <bb/core/messages.h>
#include <bb/core/conv_utils.h>
#include <bb/core/PersistentSequenceNumber.h>
#include <bb/core/DatedFilenameFormatter.h>

#include "step.h"

using namespace std;
namespace bb {
namespace step {

int gsettings::verbose_mode = 1;

namespace
{
    const date_t g_todayDate = date_t::today();

    source_t g_source;

    ISendTransportPtr g_sendTransport;
    uint32_t g_sendSequence;
    uint32_t g_splitSequence;

    struct ValueUpdater
    {
        ValueUpdater(const std::string &value)
            : value(value)
        {
        }
        void operator()(TagValue &tv)
        {
            tv.value = value;
        }

        std::string value;
    };
}

TagValue::TagValue(Tag t, const std::string &v)
    : tag(t), value(v)
{
}

Message::Message(bool isLogon)
{
    m_tagValue.push_back(TagValue(BEGIN_STRING, "FIXT.1.1"));
    m_tagValue.push_back(TagValue(BODY_LENGTH, "0"));
    m_tagValue.push_back(TagValue(CHECK_SUM, "0"));

    if (isLogon) {
        add(MSG_TYPE, "A");
    } else {
        add(MSG_TYPE, "0");
    }
}

Message::~Message()
{
}

void Message::add(Tag tag, const std::string &value)
{
    // inserting a new tag before the checksum tag
    TagMap::nth_index<1>::type &valueByTag = m_tagValue.get<1>();
    valueByTag.insert(--valueByTag.end(), TagValue(tag, value));
    std::ostringstream oss;
    oss << tag << '=' << value;
    std::string currentLen = get(BODY_LENGTH);
    update(BODY_LENGTH, boost::lexical_cast<std::string>(
                boost::lexical_cast<int>(currentLen) + oss.str().size() + 1));
}

std::string Message::get(Tag tag) const
{
    const TagMap::nth_index<1>::type &valueByTag = m_tagValue.get<1>();
    TagMap::nth_index<1>::type::iterator itr = valueByTag.find(tag);
    if (itr != valueByTag.end())
    {
        return itr->value;
    }
    return std::string();
}

void Message::update(Tag tag, const std::string &value)
{
    TagMap::nth_index<1>::type &valueByTag = m_tagValue.get<1>();
    TagMap::nth_index<1>::type::iterator itr = valueByTag.find(tag);
    if (itr != valueByTag.end())
    {
        valueByTag.modify(itr, ValueUpdater(value));
    }
}

void Message::write(bb::ByteSinkPtr sink)
{
    std::stringstream ss;
    uint64_t checksum = 0;

    for (TagMap::const_iterator itr = m_tagValue.begin(); itr != m_tagValue.end(); ++itr)
    {
        std::ostringstream oss;
        oss << itr->tag << '=' << itr->value;
        std::string str = oss.str();
        const char *content = str.c_str();
        size_t len = str.size();
        if (itr->tag != CHECK_SUM)
        {
            for (size_t i = 0; i < len; ++i)
            {
                ss << content[i];
                checksum += content[i];
            }
            checksum += SOH;
            ss << "|";
            sink->write(content, len);
            sink->write(&SOH, 1);
        }
    }

    char checksumStr[4];
    memset(checksumStr, 0, 4);
    snprintf(checksumStr, 4, "%03d", uint32_t(checksum % 256));
    update(CHECK_SUM, checksumStr);
    sink->write("10=", 3);
    sink->write(checksumStr, 3);
    sink->write(&SOH, 1);
    sink->flush();
    ss << "10=" << checksumStr << "|";

    LOG_VERB3 << "Sent: " << ss.str() << bb::endl;
}

Buffer::Buffer(Buffer *previous, const char *d, size_t l)
    : data(d), length(l)
{
    previous->next = this;
}

SZSEParser::SZSEParser( std::string& configFilename )
    : m_messages_waiting( 100000 )  // pre-alocate buffer to process 100k messages. (The max I ever seen in simulation is 2k)
{
    bb::LuaState& luaState = bb::DefaultCoreContext::getEnvironment()->luaState();
    luaState.load( configFilename.c_str() );

    try
    {
        m_decoder.create_templates( luabind::object_cast<std::string>( luaState.root()["FAST_template"] ) );
    }
    catch( luabind::cast_failed& ex )
    {
        LOG_ERROR << "FAST_template entry missing or invalid in " << configFilename << bb::endl;
        exit( EXIT_FAILURE );
    }

    g_source = source_t::make_auto(SRC_SZSE_DIRECT);
    g_source.setOrig( ORIG_SC_JYSX );
    g_source.setDest( DEST_SC_JYSX );

    g_sendSequence = 0;
    g_splitSequence = 0;


    SelectDispatcher sd;

    ConcurrentFilterTransportPtr concurrentFilterTransport( new ConcurrentFilterTransport( sd, g_source, luaState ) );

    QuoteSubscriptionMgrPtr subscriptionMgr;

    subscriptionMgr.reset( new FilterTransportSubscriptionMgr<bb::SelectDispatcherPolicy>( concurrentFilterTransport, g_source ) );
    subscriptionMgr->start();
    m_subscriptionMgrs.push_back( subscriptionMgr );

    g_sendTransport = concurrentFilterTransport;

    // TODO these messages will be ignored to improve speed
    // m_ignore_tags.insert("***");
}

void SZSEParser::decodeSTEP( unsigned char *buf, size_t buffer_size )
{
    // we only care about these tags
    std::string tag35MsgType;
    std::string tag52SendingTime;
    std::string tag95RawDataLength;

    bool inTag = true;
    size_t tagLen = 0, valueLen = 0;
    unsigned char *tagStart = buf, *valueStart = NULL;

    // LOG_INFO << "message info, content=" << os.str() << bb::endl;
    for (size_t i = 0; i < buffer_size; ++i)
    {
        char ch = buf[i];
        if (ch != SOH)
        {
            if (ch == '=')
            {
                // end of the tag
                inTag = false;
                valueStart = buf + (i + 1);

                if (memcmp(tagStart, "96", 2) == 0 &&
                    !tag95RawDataLength.empty() &&
                    !tag35MsgType.empty() &&
                    !tag52SendingTime.empty())
                {
                    // got everything we need to decode the FAST payload
                    unsigned int data_size = boost::lexical_cast<unsigned int>( tag95RawDataLength );
                    decodeFAST( tag35MsgType, tag35MsgType, valueStart, data_size );
                    return;
                }
            }
            else
            {
                if (inTag)
                {
                    ++tagLen;
                }
                else
                {
                    ++valueLen;
                }
            }
        }
        else
        {
            if( tagLen == 2 )
            {
                if( memcmp( tagStart, "35", 2 ) == 0 )
                {
                    tag35MsgType.assign( reinterpret_cast<char*>(valueStart), valueLen );

                    // if STEP tag is in the ignore list then skip it
                    if( m_ignore_tags.find( tag35MsgType ) != m_ignore_tags.end() )
                    {
                        return;
                    }
                }
                else if( memcmp( tagStart, "52", 2 ) == 0 )
                {
                    tag52SendingTime.assign( reinterpret_cast<char*>(valueStart), valueLen );
                }
                else if( memcmp( tagStart, "95", 2 ) == 0 )
                {
                    tag95RawDataLength.assign( reinterpret_cast<char*>(valueStart), valueLen );
                }
            }

            // reset the state
            tagStart = buf + i + 1;
            tagLen = valueLen = 0;
            inTag = true;
        }
    }
}

void SZSEParser::decodeFAST( std::string& tag35MsgType, std::string& tag52SendingTime, unsigned char* payload, size_t len )
{
    string content((const char*)payload, len);

    LOG_VERB10 << tag52SendingTime << " Going to decode " << tag35MsgType  << " data_size:" << len << bb::endl;
    const size_t message_count = m_decoder.decode_buffer( static_cast<CBytes::byte*>( payload ), len, m_messages_waiting );

    for( size_t i = 0; i < message_count; ++i )
    {
        const FIX::Message& fix_msg = m_messages_waiting[ i ];
        LOG_VERB10 << fix_msg.toXML() << bb::endl;

        const std::string& templateId = fix_msg.getHeader().getField( 35 );

        if (templateId == "4101") { // market snap
            decodeMarketSnap(fix_msg);
        } else if (templateId == "4201") { // market trade by trade 
            decodeMarketTbt(fix_msg);
        } else if (templateId == "4202") { // tick trade by trade
            decodeTickTbt(fix_msg);
        } else {
            // LOG_WARN << templateId << " not processed" << bb::endl;
            continue;
        }
        continue;
    }
}

void SZSEParser::decodeMarketSnap(const FIX::Message& fix_msg) {
    instrument_t instr = bb::instrument_t::unknown();
   
    // clear
    m_szseMarketSnapCommon.clear();
    m_szseMarketSnapBid.clear();
    m_szseMarketSnapAsk.clear();

    g_splitSequence++;

    // initialize
    m_szseMarketSnapCommon->mutable_slit_info()->set_seq(g_splitSequence);
    m_szseMarketSnapCommon->mutable_slit_info()->set_bodytype_mask(1);

    m_szseMarketSnapBid->mutable_slit_info()->set_seq(g_splitSequence);
    m_szseMarketSnapBid->mutable_slit_info()->set_bodytype_mask(2);

    m_szseMarketSnapAsk->mutable_slit_info()->set_seq(g_splitSequence);
    m_szseMarketSnapAsk->mutable_slit_info()->set_bodytype_mask(4);

    BOOST_FOREACH(const FIX::FieldMap::Fields::value_type& field, fix_msg) {
        const int& field_id = field.first;
        const std::string& field_value = field.second.getString();
        switch (field_id) {
            case 42: { // OrigTime 
                m_szseMarketSnapCommon->set_data_timestamp(parse_time_HHMMSSmm(g_todayDate, field_value).toDouble()); 
            } break;
            case 10201: { // ChannelNo 
                m_szseMarketSnapCommon->set_channel_no(convert_field<int64_t>(field_value, 0));
            } break;
            case 1500: { // MDStreamID 
                string str = field_value; str.resize(3);
                m_szseMarketSnapCommon->set_md_stream_id(str);
            } break;
            case 48: { // SecurityID 
                // TODO fix SZSE tag later
                instr = instrument_t::fromString("SZSE_" + field_value);
                if( !instr.is_valid() )
                {
                    // LOG_INFO << "decodeMarketSnap, unknown szseCode: " << field_value << bb::endl;
                }

                string str = field_value; str.resize(8);

                m_szseMarketSnapCommon->set_symid(instr.sym.id());
                m_szseMarketSnapBid->set_symid(instr.sym.id());
                m_szseMarketSnapAsk->set_symid(instr.sym.id());

                m_szseMarketSnapCommon->set_security_id(str);
                m_szseMarketSnapBid->set_security_id(str);
                m_szseMarketSnapAsk->set_security_id(str);
            } break;
            case 22: { // SecurityIDSource 
                string str = field_value; str.resize(4);
                m_szseMarketSnapCommon->set_security_id_source(str);
            } break;
            case 8538: { // TradingPhaseCode 
                string str = field_value; str.resize(2);
                m_szseMarketSnapCommon->set_trading_phase_code(str);
            } break;
            case 140: { // PrevClosePx, precision=4
                m_szseMarketSnapCommon->set_prev_close_px(convert_field<double>(field_value, 0) / 10000);
            } break;
            case 8503: { // NumTrades 
                m_szseMarketSnapCommon->set_num_trades(convert_field<int64_t>(field_value, 0));
            } break;
            case 387: { // TotalVolumeTrade P=2
                m_szseMarketSnapCommon->set_total_volume_trade(convert_field<int64_t>(field_value, 0) / 100);
            } break;
            case 8504: { // TotalValueTrade P=4
                m_szseMarketSnapCommon->set_total_value_trade(convert_field<double>(field_value, 0) / 10000);
            } break;
            case 10207: { // StockNum
                m_szseMarketSnapCommon->set_total_value_trade(convert_field<uint32_t>(field_value, 0));
            } break;
        }
    }

    for( FIX::FieldMap::g_iterator fd = fix_msg.g_begin(); fd != fix_msg.g_end(); ++fd )
    {
        const std::vector<FIX::FieldMap*>& vec = fd->second;

        // repeated bid and ask
        BOOST_FOREACH(const FIX::FieldMap* fields, vec)
        {
            string entryType;
            double entryPx;
            int64_t entrySize;
            int64_t numOfOrders;

            BOOST_FOREACH(const FIX::FieldMap::Fields::value_type& field, *fields)
            {
                const int& sub_id = field.first;
                const std::string& sub_value = field.second.getString();

                switch( sub_id )
                {
                    case 269: { // entryType 
                        entryType = sub_value;
                    } break;
                    case 270: { // entryPx 
                        entryPx = convert_field<double>(sub_value, 0);
                    } break;
                    case 271: { // entrySize 
                        entrySize = convert_field<double>(sub_value, 0);
                    } break;
                    case 1023: {// priceLevel 
                        // priceLevel = convert_field<uint32_t>(sub_value, 0);
                    } break;
                    case 346: { // numOfOrders 
                        numOfOrders = convert_field<int64_t>(sub_value, 0);
                    } break;
                }
            }

            // precision
            entryPx /= 1000000;
            entrySize /= 100;

            acr::SzseMarketSnap::DepthLevel* depthLevelBid = NULL;
            acr::SzseMarketSnap::DepthLevel* depthLevelAsk = NULL;
            if (string("0") == entryType) { // bid level
                depthLevelBid = m_szseMarketSnapBid->add_bid();
                depthLevelBid->set_price(entryPx);
                depthLevelBid->set_size(entrySize);
                depthLevelBid->set_number_of_orders(numOfOrders);
            } else if (string("1") == entryType) { // ask level
                depthLevelAsk = m_szseMarketSnapAsk->add_ask();
                depthLevelAsk->set_price(entryPx);
                depthLevelAsk->set_size(entrySize);
                depthLevelAsk->set_number_of_orders(numOfOrders);
            } else {
                if (string("2") == entryType) { //TODO strncmp
                    m_szseMarketSnapCommon->set_latest_price(entryPx);
                } else if (string("3") == entryType) {
                    m_szseMarketSnapCommon->set_current_index(entryPx);
                } else if (string("4") == entryType) {
                    m_szseMarketSnapCommon->set_open_price(entryPx);
                } else if (string("7") == entryType) {
                    m_szseMarketSnapCommon->set_high_price(entryPx);
                } else if (string("8") == entryType) {
                    m_szseMarketSnapCommon->set_low_price(entryPx);
                } else if (string("x1") == entryType) {
                    m_szseMarketSnapCommon->set_up_down_1(entryPx);
                } else if (string("x2") == entryType) {
                    m_szseMarketSnapCommon->set_up_down_2(entryPx);
                } else if (string("x3") == entryType) {
                    m_szseMarketSnapCommon->set_bid_stat_avr_px(entryPx);
                    m_szseMarketSnapCommon->set_bid_stat_total_vol(entrySize);
                } else if (string("x4") == entryType) {
                    m_szseMarketSnapCommon->set_ask_stat_avr_px(entryPx);
                    m_szseMarketSnapCommon->set_ask_stat_total_vol(entrySize);
                } else if (string("x5") == entryType) {
                    m_szseMarketSnapCommon->set_pe_ratio_1(entryPx);
                } else if (string("x6") == entryType) {
                    m_szseMarketSnapCommon->set_pe_ratio_2(entryPx);
                } else if (string("x7") == entryType) {
                    m_szseMarketSnapCommon->set_net_value(entryPx);
                } else if (string("x8") == entryType) {
                    m_szseMarketSnapCommon->set_live_net_value(entryPx);
                } else if (string("x9") == entryType) {
                    m_szseMarketSnapCommon->set_premium_rate(entryPx);
                } else if (string("xa") == entryType) {
                    m_szseMarketSnapCommon->set_prev_close_index(entryPx);
                } else if (string("xb") == entryType) {
                    m_szseMarketSnapCommon->set_open_index(entryPx);
                } else if (string("xc") == entryType) {
                    m_szseMarketSnapCommon->set_high_index(entryPx);
                } else if (string("xd") == entryType) {
                    m_szseMarketSnapCommon->set_low_index(entryPx);
                } else if (string("xe") == entryType) {
                    m_szseMarketSnapCommon->set_high_limit_price(entryPx);
                } else if (string("xf") == entryType) {
                    m_szseMarketSnapCommon->set_low_limit_price(entryPx);
                } else if (string("xg") == entryType) {
                    m_szseMarketSnapCommon->set_open_interest(entrySize);
                } else if (string("xh") == entryType) {
                    m_szseMarketSnapCommon->set_disk_price(entryPx);
                } else if (string("xi") == entryType) {
                    m_szseMarketSnapCommon->set_reference_price(entryPx);
                }
            }

            for( FIX::FieldMap::g_iterator k = (*fields).g_begin() ; k != (*fields).g_end(); ++k )
            {
                int index = 0;
                const std::vector<FIX::FieldMap*>& vecc = k->second;
                BOOST_FOREACH( const FIX::FieldMap* fmm, vecc )
                {
                    // TODO limit the orders count, in order to not beyond proto message max payload, and also need log
                    // something for future enhance
                    if ((index++) >= 5) {
                        LOG_INFO << "decodeMarketSnap, field size exceed limit(5), "
                                 << "symid=" << instr.sym.id()
                                 << "realsize=" << vecc.size()
                                 << bb::endl;
                        break;
                    }

                    BOOST_FOREACH(const FIX::FieldMap::Fields::value_type& fdd, *fmm)
                    {
                        const int& sub_sub_id = fdd.first;
                        const std::string& sub_sub_value = fdd.second.getString();
                        if (sub_sub_id == 38) { // orderQty
                            if (depthLevelBid) {
                                depthLevelBid->add_orders(convert_field<int64_t>(sub_sub_value, 0) / 100);
                            }
                            if (depthLevelAsk) {
                                depthLevelAsk->add_orders(convert_field<int64_t>(sub_sub_value, 0) / 100);
                            }
                        }
                    }
                }
            }
        }
    }

    // dispatch messages
    {
        m_szseMarketSnapCommon.setHeader(instr.sym, g_source, g_sendSequence++);
        m_szseMarketSnapCommon.pack();
        g_sendTransport->send(&m_szseMarketSnapCommon);
    }
    {
        m_szseMarketSnapBid.setHeader(instr.sym, g_source, g_sendSequence++);
        m_szseMarketSnapBid.pack();
        g_sendTransport->send(&m_szseMarketSnapBid);
    }
    {
        m_szseMarketSnapAsk.setHeader(instr.sym, g_source, g_sendSequence++);
        m_szseMarketSnapAsk.pack();
        g_sendTransport->send(&m_szseMarketSnapAsk);
    }
}

void SZSEParser::decodeMarketTbt(const FIX::Message& fix_msg) {
    m_szseMarketTbt.clear();
    instrument_t instr = bb::instrument_t::unknown();

    BOOST_FOREACH(const FIX::FieldMap::Fields::value_type& field, fix_msg) {
        const int& field_id = field.first;
        const std::string& field_value = field.second.getString();
        switch (field_id) {
            case 10201: { // ChannelNo
                m_szseMarketTbt->set_channel_no(convert_field<uint32_t>(field_value, 0));
            } break;
            case 1181: { // ApplSeqNum
                m_szseMarketTbt->set_appl_seq_num(convert_field<int64_t>(field_value, 0));
            } break;
            case 1500: { // MDStreamID
                string str = field_value;
                str.resize(3);
                m_szseMarketTbt->set_md_stream_id(str);
            } break;
            case 48: { // SecurityID
                instr = instrument_t::fromString("SZSE_" + field_value);
                if( !instr.is_valid() )
                {
                    // LOG_INFO << "decodeMarketSnap, unknown szseCode: " << field_value << bb::endl;
                }

                m_szseMarketTbt->set_symid(instr.sym.id());

                string str = field_value; str.resize(8);
                m_szseMarketTbt->set_security_id(str);
            } break;
            case 22: { // SecurityIDSource
                string str = field_value; str.resize(4);
                m_szseMarketTbt->set_security_id_source(str);
            } break;
            case 44: { // Price P=4
                m_szseMarketTbt->set_price(convert_field<double>(field_value, 0) / 10000);
            } break;
            case 38: { // OrderQty P=2
                m_szseMarketTbt->set_order_qty(convert_field<int64_t>(field_value, 0) / 100);
            } break;
            case 54: { // Side
                string str = field_value; str.resize(1);
                m_szseMarketTbt->set_side(str);
            } break;
            case 40: { // OrderType
                string str = field_value; str.resize(1);
                m_szseMarketTbt->set_order_type(str);
            } break;
            case 59: { // TimeInForce
                m_szseMarketTbt->set_time_in_force(convert_field<uint32_t>(field_value, 0));
            } break;
            case 1090: { // MaxPriceLevels
                m_szseMarketTbt->set_max_price_levels(convert_field<uint32_t>(field_value, 0));
            } break;
            case 110: { // MinQty
                m_szseMarketTbt->set_min_qty(convert_field<int64_t>(field_value, 0));
            } break;
            case 664: { // ConfirmId
                string str = field_value; str.resize(8);
                m_szseMarketTbt->set_confirm_id(str);
            } break;
            case 8911: { // ExpirationDays
                m_szseMarketTbt->set_expiration_days(convert_field<uint32_t>(field_value, 0));
            } break;
            case 8906: { // ExpirationType
                m_szseMarketTbt->set_expiration_type(convert_field<uint32_t>(field_value, 0));
            } break;
            case 60: { // TransactionTime
                m_szseMarketTbt->set_transact_time(parse_time_HHMMSSmm(g_todayDate, field_value).toDouble()); 
            } break;
            case 10184: { // Contactor
                string str = field_value; str.resize(12);
                m_szseMarketTbt->set_contactor(str);
            } break;
            case 10185: { // ContactInfo
                string str = field_value; str.resize(30);
                m_szseMarketTbt->set_contact_info(str);
            } break;
        }
    }

    // dispatch messages
    m_szseMarketTbt.setHeader(instr.sym, g_source, g_sendSequence++);
    m_szseMarketTbt.pack();
    g_sendTransport->send(&m_szseMarketTbt);
}

void SZSEParser::decodeTickTbt(const FIX::Message& fix_msg) {
    m_szseTickTbt.clear();
    instrument_t instr = bb::instrument_t::unknown();

    BOOST_FOREACH(const FIX::FieldMap::Fields::value_type& field, fix_msg) {
        const int& field_id = field.first;
        const std::string& field_value = field.second.getString();
        switch (field_id) {
            case 10201: { // ChannelNo
                m_szseTickTbt->set_channel_no(convert_field<uint32_t>(field_value, 0));
            } break;
            case 1181: { // ApplSeqNum
                m_szseTickTbt->set_appl_seq_num(convert_field<int64_t>(field_value, 0));
            } break;
            case 1500: { // MDStreamID
                string str = field_value;
                str.resize(3);
                m_szseTickTbt->set_md_stream_id(str);
            } break;
            case 10116: { // BidApplSeqNum
                m_szseTickTbt->set_bid_appl_seq_num(convert_field<int64_t>(field_value, 0));
            } break;
            case 10117: { // OfferApplSeqNum
                m_szseTickTbt->set_offer_appl_seq_num(convert_field<int64_t>(field_value, 0));
            } break;
            case 48: { // SecurityID
                instr = instrument_t::fromString("SZSE_" + field_value);
                if( !instr.is_valid() )
                {
                    // LOG_INFO << "decodeMarketSnap, unknown szseCode: " << field_value << bb::endl;
                }

                m_szseTickTbt->set_symid(instr.sym.id());
                string str = field_value; str.resize(8);
                m_szseTickTbt->set_security_id(str);
            } break;
            case 22: { // SecurityIDSource
                string str = field_value; str.resize(4);
                m_szseTickTbt->set_security_id_source(str);
            } break;
            case 31: { // LastPx P=4
                m_szseTickTbt->set_last_price(convert_field<double>(field_value, 0) / 10000);
            } break;
            case 32: { // LastQty
                m_szseTickTbt->set_last_qty(convert_field<int64_t>(field_value, 0) / 100);
            } break;
            case 150: { // ExecType
                string str = field_value; str.resize(1);
                m_szseTickTbt->set_exec_type(str);
            } break;
            case 60: { // TransacTime
                m_szseTickTbt->set_transact_time(parse_time_HHMMSSmm(g_todayDate, field_value).toDouble()); 
            } break;
        }
    }

    // dispatch messages
    m_szseTickTbt.setHeader(instr.sym, g_source, g_sendSequence++);
    m_szseTickTbt.pack();
    g_sendTransport->send(&m_szseTickTbt);
}

template<typename T>
T SZSEParser::convert_field( const std::string& field, T error_value, bool log_warn ) const
{
    try
    {
        return boost::lexical_cast<T>( field );
    }
    catch(const boost::bad_lexical_cast &)
    {
        if( log_warn )
        {
            LOG_WARN << "Could not convert " << field << " into " << typeid(T).name() << ". Using default value of " << error_value << bb::endl;
        }
        return error_value;
    }
}

namespace {
const uint64_t YEAR_MASK = 10000000000000;
const uint64_t MONTH_MASK = 100000000000;
const uint64_t DAY_MASK = 1000000000;
const uint64_t HOUR_MASK = 10000000;
const uint64_t MIN_MASK = 100000;
const uint64_t SEC_MASK = 1000;
};

// orignal format: 20161208093554800
timeval_t SZSEParser::parse_time_HHMMSSmm(const date_t& todayDate, const std::string& field ) const
{
    uint64_t value = convert_field<uint64_t>(field, 0);

    int year = value / YEAR_MASK;
    value -= (YEAR_MASK * year);

    int mon = value / MONTH_MASK;
    value -= (MONTH_MASK * mon);

    int day = value / DAY_MASK;
    value -= (DAY_MASK * day);

    int hour = value / HOUR_MASK;
    value -= (HOUR_MASK * hour);

    int min = value / MIN_MASK;
    value -= (MIN_MASK * min);

    int sec = value / SEC_MASK;
    value -= (SEC_MASK * sec);

    return timeval_t::make_time(year, mon, day, hour, min, sec, (value * 1000));
}

TemplateBuilder FASTDecoder::s_templateBuilder;

FASTDecoder::FASTDecoder()
    : m_encoder( TransferEncoder::get_instance() )
{
}

void FASTDecoder::create_templates( const std::string& template_file )
{
    codec = s_templateBuilder.create_templates( template_file, templates, &m_encoder );
}

size_t FASTDecoder::decode_buffer( CBytes::byte* buffer, size_t size, std::vector<FIX::Message>& output )
{
    m_encoder.set_input_stream( buffer, size );
    m_encoder.reset_input_pointer();
    return decode( output );
}
}
}
