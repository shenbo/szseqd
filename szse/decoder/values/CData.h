#ifndef BB_SERVERS_SHSE_QD_DECODER_VALUES_CDATA_H
#define BB_SERVERS_SHSE_QD_DECODER_VALUES_CDATA_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */



/**
 * This is the base class for all types of decoded data.  CData instances can
 * be null and may have a length.
 */
class CData {
  protected:
    bool _null;
    int _length;

    CData() {
      _null = true;
      _length = 0;
    }


  public:
    virtual ~CData() {
      _null = true;
      _length = 0;
    }


    /**
     * @return the number of bytes occupied by this data object.
     */
    unsigned int get_length() const {
      return _length;
    }


    /**
     * Set the number of bytes represented by this object.
     *
     * @param length the length of the data contained in this object.
     */
    void set_length(const int length) {
      _length = length;
    }


    /**
     * @return true if this object contains null data or is null itself.
     */
    bool is_null() const {
      return _null;
    }


    /**
     * Set the internal state representing whether the internal data is null or
     * not.
     *
     * @param null true if this CData is null.
     */
    void set_null(const bool null) {
      _null = null;
    }
};


#endif // BB_SERVERS_SHSE_QD_DECODER_VALUES_CDATA_H
