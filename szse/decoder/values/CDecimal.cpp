// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "../FASTDecoder.h"



void CDecimal::set_value(const string & value) {
  CValue<string>::set_value(value);
  const string::size_type dot = value.find_first_of('.');
  _mantissa = CInt64::parse(dot == string::npos ? value : string(value).erase(dot, 1));
  _exponent = (dot == string::npos) ? 0 : (1 - value.length() + dot);
}


string CDecimal::get_decimal() const {
  static stringstream ss;

  string out = Utils::to_string( abs( _mantissa ) );
  const int index = out.length() + _exponent;
  if (index == (int) out.length()) return out;
  if (index >= (int) out.length()) {
    ss.str("");
    ss << out << Utils::zeros(_exponent);
    return ss.str();
  }
  if (index > 0) return out.insert(index, ".");
  std::string zero_prefix( _mantissa >=0 ? "0." : "-0." );
  if (index == 0) return out.insert(0, zero_prefix);
  ss.str("");
  ss << zero_prefix << Utils::zeros(0 - index) << out;
  return ss.str();
}


ostream & operator << (ostream & out, const CDecimal & decimal) {
  return decimal.insert(out);
}

