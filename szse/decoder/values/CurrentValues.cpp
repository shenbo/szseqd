// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "../FASTDecoder.h"


ostream & operator<<(ostream & out, const CurrentValues & cvs) {
  return cvs.insert(out);
}
