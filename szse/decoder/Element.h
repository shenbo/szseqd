#ifndef BB_SERVERS_SHSE_QD_DECODER_ELEMENT_H
#define BB_SERVERS_SHSE_QD_DECODER_ELEMENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <set>
#include <string>
#include <vector>


class TiXmlElement;


/**
 * This class serves as a wrapper for the information contained in an XML
 * element.  There is minimal implementation-specific dependencies, so you
 * are free to implement your XML parsing with your library of choice.
 * Just as long as you populate the Element objects using the values.
 */
class Element
{
    typedef Element* ElementPtr; // has to be raw pointer or else TemplateBuilder.cpp can't use it
    typedef std::vector<ElementPtr> Children;

public:
    /**
     * An Element iterator.
     */
    typedef Children::iterator Iterator;

    /**
     * @return the root element of the XML file argument.
     */
    static Element * get_root(const std::string & template_filename);

    /**
     * @return the name of this Element.
     */
    std::string get_name() const { return name; }

    /**
     * @return an iterator pointed at the first element.
     */
    Iterator children_begin() { return children.begin(); }

    /**
     * @return an iterator pointed <i>after</i> the last element.
     */
    Iterator children_end() { return children.end(); }

    /**
     * @param name the name of the argument to access.
     * @return the value of the attribute matching the argument.
     */
    std::string attribute(const std::string & name) { return attributes[name]; }

    /**
     * Deallocates the memory created on the heap for this Element.
     */
    ~Element();

private:
    /**
     * Recursively builds an Element and children Elements.
     * All attributes are saved to the map.
     */
    Element(TiXmlElement * element, Element * parent = NULL);

    Element * parent;
    std::string name;
    Children children;
    std::map<std::string, std::string> attributes;
};

#endif // BB_SERVERS_SHSE_QD_DECODER_ELEMENT_H
