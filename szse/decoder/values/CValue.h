#ifndef BB_SERVERS_SHSE_QD_DECODER_VALUES_CVALUE_H
#define BB_SERVERS_SHSE_QD_DECODER_VALUES_CVALUE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <assert.h>
#include <iostream>

#include "CData.h"

using std::ostream;


/**
 * This class provides a generic way to set and get values of any type.
 * Currently it is used for numeric and string types only.
 */
template <typename value_type>
class CValue : public CData {
  public:
    // Note: This is typedefed publicly for easy reference in other classes:
    typedef value_type type;

  protected:
    type _value;

    CValue() {
      // nothing to do!
    }

  public:
    virtual ~CValue() {
      // nothing to do!
    }


    /**
     * @return the value of this CValue or 0 if this CValue is null.
     */
    virtual type get_value() const {
      return _null ? 0 : _value;
    }


    /**
     * Sets the internal value to the argument value.  After this method
     * has been called this CValue is no longer null.
     *
     * @param value the value to set this CValue to.
     */
    virtual void set_value(const type & value) {
      _null = false;
      _value = value;
    }


    /**
     * Sets the internal state of this CValue to null.
     */
    virtual void reset() {
      _null = true;
    }


    virtual ostream & insert(ostream & out) const {
      out << "[null=" << (_null ? "true" : "false") << ",value=" << _value << "]";
      return out;
    }

  protected:
    /**
     * Disabled.
     */
    CValue(const CValue & other) { assert(0); }
};


#endif // BB_SERVERS_SHSE_QD_DECODER_VALUES_CVALUE_H
