// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "../FASTDecoder.h"


ostream & operator << (ostream & out, const CBytes & bytes) {
  static char buffer[3];

  out << "CBytes[" 
      << (bytes.is_null() ? "null," : "")
      << "length=" << bytes.get_length() << ","
      << "values={";
  for (int i = 0, j = bytes.get_length(); i != j; ++i) {
    sprintf(buffer, "%02X", bytes.get_byte(i));
    out << buffer << (i + 1 != j ? ' ' : '}');
  }
  out << "]";
  return out;
}
