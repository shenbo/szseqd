
sender_comp_id = "SHANCE"
target_comp_id = "F000618M0001" -- production config

FAST_template = "fast_template.xml"

filter_config = {
    verbose = true,
    sequence = { file = "/var/tmp/shseqd.seq", concurrent = true },
    filtered_symbols = {
    },
    --filtered_mtypes = {
    --},
    msglogd = {
        directory = "/local/bb/logdir/",
        compress = true,
        sink_config = {
            type           = 'producer',
            buffer_num     = 100,
            buffer_size    = 100*1024,
            -- don't flush intra-day: see ASIA-596
            flush_interval = 80000,
        },
    },
}

