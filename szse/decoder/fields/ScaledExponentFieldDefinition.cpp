// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "../FASTDecoder.h"

ScaledExponentFieldDefinition::ScaledExponentFieldDefinition(const int id, 
  const int t, const int op, const int tid, const int s, const bool optional)
    :I32FieldDefinition(id, t, op, tid, s, optional)
{
  // nothing else to do!
}


void ScaledExponentFieldDefinition::decode(const int slot, Codec & codec, FIX::FieldMap& output) {
  reset_data();
  
  if (Decoder::LOGME) {
    FLogger::get_instance().set_id(Utils::to_string(get_field_id()));
    FLogger::get_instance().set_field(get_field_type_name());
    FLogger::get_instance().set_operator(get_field_operator_name());
    FLogger::get_instance().set_optional(is_optional() ? "true" : "false");
    if (is_mandatory()) {
      FLogger::get_instance().set_pmap("M");
    } else {
      FLogger::get_instance().set_pmap((codec.get_presence_map().get_bit(slot)) ? "1" : "0");
    }
    CurrentValues & cvs = codec.get_current_values();
    bool valid = cvs.is_valid(get_slot());
    string currentValue = valid ? Utils::to_string(cvs.get_i32(get_slot())) : "null";
    FLogger::get_instance().set_current(currentValue);
  }
  
  codec.decode(slot, (CInt32 *) get_data());
  
  if (Decoder::LOGME) {
    FLogger::get_instance().set_fix("");
    FLogger::get_instance().pretty_print_display();
    FLogger::get_instance().reset();
  }
  
  return;
}
