// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "../FASTDecoder.h"


void StringFieldDefinition::decode(const int slot, Codec & codec, FIX::FieldMap& output) {
  _value -> reset();
  
  if (Decoder::LOGME) {
    FLogger::get_instance().set_id(Utils::to_string(get_field_id()));
    FLogger::get_instance().set_field(get_field_type_name());
    FLogger::get_instance().set_operator(get_field_operator_name());
    FLogger::get_instance().set_optional(is_optional() ? "true" : "false");
    if (is_mandatory()) {
      FLogger::get_instance().set_pmap("M");
    } else {
      FLogger::get_instance().set_pmap((codec.get_presence_map().get_bit(slot)) ? "1" : "0");
    }
    
    CurrentValues & cvs = codec.get_current_values();
    string currentValue = cvs.is_valid(get_slot()) ? cvs.get_string(get_slot()) : "null";
    FLogger::get_instance().set_current(currentValue);
  }
  
  codec.decode(slot, _value);
  
  if (_value -> is_null()) {
    if (Decoder::LOGME) {
      FLogger::get_instance().set_value("null");
      FLogger::get_instance().pretty_print_display();
      FLogger::get_instance().reset();
    }
    return ;
  }

  output.setField( get_field_id(),  _value -> get_value() );

}


void StringFieldDefinition::set_current_value(Codec & codec, const string & data) {
  codec.get_current_values().set_string(get_slot(), data);
  set_default_value(true);
}

string StringFieldDefinition::get_value() {
  return _value -> get_value();
}

CData * StringFieldDefinition::get_data() {
  return _value;
}

void StringFieldDefinition::reset_data() {
  _value -> reset();
}

ostream & StringFieldDefinition::insert(ostream & out) const {
  FieldDefinition::insert(out);
  out << ".StringFieldDefinition[" << "null=" << (_value -> is_null() ? "true" : "false") << ",value='" << _value -> get_value() << "']";
  return out;
}
