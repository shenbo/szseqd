// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "../FASTDecoder.h"
#include <quickfix/Group.h>

typedef Codec::Definitions::iterator Iterator;


GroupFieldDefinition::GroupFieldDefinition(int id, int type, int op, int tid, int s, Codec & codec, bool optional)
  :FieldDefinition(id, type, op, tid, s, optional), group_codec(codec)
{
  presence_map = false;

  // check to see if a presence map is required for this group
  Codec::Definitions & definitions = codec.get_definitions();
  for (Iterator i = ++definitions.begin(), e = definitions.end(); i != e; ++i) {
    if (!Utils::mandatory_field((*i) -> get_field_operator(), (*i) -> is_optional())) {
      presence_map = true;
      break;
    }
  }

  // if the group is optional notify the group length
  if (is_optional()) {
    definitions[0] -> set_optional(true);
  }
}


void GroupFieldDefinition::decode(const int slot, Codec & codec, FIX::FieldMap& output) {
    Codec::Definitions & definitions = group_codec.get_definitions();

    U32FieldDefinition * gfd = (U32FieldDefinition *) definitions.at(0);
    gfd -> reset_data();
    gfd -> set_slot(0); // TODO - make sure this is correct - was set_slot(slot)
    group_length = CuInt32();

    if (Decoder::LOGME) {
        FLogger::get_instance().set_id(Utils::to_string(gfd -> get_field_id()));
        FLogger::get_instance().set_field(gfd -> get_field_type_name());
        FLogger::get_instance().set_operator(gfd -> get_field_operator_name());
        FLogger::get_instance().set_optional(gfd -> is_optional() ? "true" : "false");
        FLogger::get_instance().set_pmap("M");

        string currentValue = (codec.get_current_values().is_valid(get_slot())) ?
            Utils::to_string(codec.get_current_values().get_u32(slot)) : "null";
        FLogger::get_instance().set_current(currentValue);
    }
    // We assume that the length field is mandatory, hence presence is assumed below:
    codec.decode((CuInt32 *) gfd -> get_data(), gfd, true);
    group_length = *((CuInt32 *) gfd -> get_data());

    if (Decoder::LOGME) {
        stringstream ss2;
        ss2 << gfd -> get_field_id() << "=" << gfd -> get_value();
        FLogger::get_instance().set_fix(ss2.str());
        FLogger::get_instance().pretty_print_display();
        FLogger::get_instance().reset();
    }

    if (gfd -> get_data() -> is_null()) return;

    for (unsigned int i = 0; i < group_length.get_value(); ++i) {
        FIX::Group group( gfd ->get_field_id(), FIX::message_order::normal );
        CBytes * pmap = (presence_map) ? group_codec.get_presence_map_off_stream() : 0;
        group_codec.parse_pmap(pmap, presence_map, 1, false);

        if (Decoder::LOGME) {
            FLogger::get_instance().print_buffer();
            FLogger::get_instance().reset_buffer();
        }

        for (int slot = 1, last_slot = definitions.size(); slot < last_slot; ++slot) {
            definitions[slot] -> decode(slot, group_codec, group);
        }

        output.addGroup( gfd ->get_field_id(), group );
    }

    return ;
}

