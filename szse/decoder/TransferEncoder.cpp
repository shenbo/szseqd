// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "FASTDecoder.h"


TransferEncoder* TransferEncoder::instance = new TransferEncoder();


TransferEncoder::TransferEncoder()
  :parse_buffer(new CBytes(PARSE_BUFFER_SIZE))
{
  bytes = parse_buffer -> get_value();
}


TransferEncoder::~TransferEncoder() {
  if (parse_buffer != 0) delete parse_buffer; 
  parse_buffer = 0;
}


/**
 * Sets the value of the argument CBytes using the data in the _input field.
 */
void TransferEncoder::parse_bytes(CBytes & data, const int size) {
  static stringstream hex;
  if (Decoder::LOGME) hex.str(""); // reset the static local buffer if logging
  
  for (int index = 0; index <= size; ++index) {

    if (Decoder::LOGME) std::cout << "Input_pointer=" << input_pointer << std::endl;

    // if done parsing:
    if (input_pointer >= input_length) {
        if (Decoder::LOGME) std::cout << "Input_pointer reached end of stream!" << std::endl;
      data.set_length(0);
      return;
    }

    if (Decoder::LOGME) hex << Utils::to_hex(_input[input_pointer]) << " ";

    // if stop bit was found:
    if (data.set_byte(index, _input[input_pointer++])) {
      if (Decoder::LOGME) FLogger::get_instance().set_hex(hex.str());
      return;
    }
  }
}


int TransferEncoder::parse_bytes() {
  parse_bytes(*parse_buffer, PARSE_BUFFER_SIZE);
  return parse_buffer -> get_length();
}


/**
 * Sets the value in the argument CString using data on the _input.
 */
void TransferEncoder::parse_string(const bool slot, CString & value) {
  static stringstream newValue, hex;
  if (Decoder::LOGME) hex.str(""); // reset the static local buffer if logging
  
  bool stopBitFound = false;
  int size = 0;
  newValue.str("");
  
  if (slot) {
    for (size = 0; !stopBitFound; ++input_pointer, ++size) {
      if (Decoder::LOGME) hex << Utils::to_hex(_input[input_pointer]) << " ";
      stopBitFound = (_input[input_pointer] & 0x80);
      newValue << ((char) (stopBitFound ? _input[input_pointer] & 0x7F : _input[input_pointer]));
    }
  } else {
    if (Decoder::LOGME) FLogger::get_instance().set_value("N/A");
  }
  
  if (Decoder::LOGME) {
    FLogger::get_instance().set_hex(hex.str());
    FLogger::get_instance().set_value(newValue.str());
  }
  
  value.set_value(newValue.str());
  value.set_length(size);
}


template<typename type>
void TransferEncoder::parse_int(CValue<type> & value, const type & sign_mask, const bool optional) {
  const int length = parse_bytes();
  value.set_length(length);
  if (length == 0) return;
  
  // reconstruct the 64 bit value:
  int64_t temp = (bytes[0] & sign_mask) ? -1 : 0;
  for (int i = 0; i < length; ++i) temp = (temp << 7) | bytes[i];
  if (optional) {
    if (temp == 0) {
      value.set_null(true); // if optional and zero, it means null!
      return;
    }
    if (!(bytes[0] & sign_mask)) --temp; // weird rule: if optional and positive we subtract 1!
  }
  value.set_value(temp);
}


template<typename type>
void TransferEncoder::parse_int(const bool slot, CValue<type> & value, const bool optional) {
  value.set_value(0);
  value.set_length(0);
  
  if (slot) {
    parse_int(value, optional);
    if (Decoder::LOGME) {
      FLogger::get_instance().set_value(Utils::to_string(value.get_value()));
    }
  } else {
    if (optional) {
      value.set_null(true);
    }
    if (Decoder::LOGME) {
      FLogger::get_instance().set_value("N/A");
      FLogger::get_instance().set_hex("");
    }
  }
}


void TransferEncoder::parse_int(const bool slot, CuInt32 & value, const bool optional) {
  CValue<CuInt32::type> & val = value;
  parse_int(slot, val, optional);
}

void TransferEncoder::parse_int(const bool slot, CInt32 & value, const bool optional) {
  CValue<CInt32::type> & val = value;
  parse_int(slot, val, optional);
}

void TransferEncoder::parse_i64(const bool slot, CInt64 & value, const bool optional) {
  CValue<CInt64::type> & val = value;
  parse_int(slot, val, optional);
}

void TransferEncoder::parse_u64(const bool slot, CuInt64 & value, const bool optional) {
  CValue<CuInt64::type> & val = value;
  parse_int(slot, val, optional);
}


void TransferEncoder::parse_int(CValue<CInt32::type> & value, const bool optional) {
  parse_int(value, (CInt32::type) SIGN_MASK_SIGNED, optional);
}

void TransferEncoder::parse_int(CValue<CuInt32::type> & value, const bool optional) {
  parse_int(value, (CuInt32::type) SIGN_MASK_UNSIGNED, optional);
}

void TransferEncoder::parse_int(CValue<CInt64::type> & value, const bool optional) {
  parse_int(value, (CInt64::type) SIGN_MASK_SIGNED, optional);
}

void TransferEncoder::parse_int(CValue<CuInt64::type> & value, const bool optional) {
  parse_int(value, (CuInt64::type) SIGN_MASK_UNSIGNED, optional);
}
