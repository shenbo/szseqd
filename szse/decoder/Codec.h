#ifndef BB_SERVERS_SHSE_QD_DECODER_CODEC_H
#define BB_SERVERS_SHSE_QD_DECODER_CODEC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <string>
#include <vector>
#include <iostream>

#include "PresenceMap.h"
#include "values/CurrentValues.h"

class FieldDefinition;
class TransferEncoder;

using std::ostream;
using std::vector;
using std::string;
using std::map;
using std::cout;
using std::endl;


class Codec {
  public:
    typedef FieldDefinition * Definition;
    typedef vector<Definition> Definitions;
    typedef map<Definition, string> DefinitionMap;
    typedef DefinitionMap::iterator Iterator;

    static const int bit_mask[];

  private:
    int _template_id;

    PresenceMap presence_map;
    TransferEncoder & serdes;
    CurrentValues cvs;
    Definitions definitions;
    DefinitionMap default_values;

    template<typename type>
    void decode_int(CValue<type> * cvalue, FieldDefinition * definition, const bool presence, vector<type> & values);

  public:
    Codec(TransferEncoder & eserdes, DefinitionMap definition_map, int template_id);
    virtual ~Codec();

    CBytes * get_presence_map_off_stream();
    int parse_pmap(CBytes * buffer, const bool present, const int start, const bool first_byte);
    void set_state(const int state);
    void decode_group_length(CInt32 & data);

    void decode(Definitions::size_type slot, CInt32 * cvalue);
    void decode(Definitions::size_type slot, CuInt32 * cvalue);
    void decode(Definitions::size_type slot, CInt64 * cvalue);
    void decode(Definitions::size_type slot, CuInt64 * cvalue);
    void decode(Definitions::size_type slot, CString * cvalue);
    void decode(Definitions::size_type slot, CDecimal * cvalue);

    void decode(CInt32 * cvalue, FieldDefinition * definition, const bool presence);
    void decode(CuInt32 * cvalue, FieldDefinition * definition, const bool presence);
    void decode(CInt64 * cvalue, FieldDefinition * definition, const bool presence);
    void decode(CuInt64 * cvalue, FieldDefinition * definition, const bool presence);

    void reset_dictionary();
    Definitions & get_definitions();
    CurrentValues & get_current_values();
    PresenceMap & get_presence_map();

    unsigned int get_template_id() const { return _template_id; }
    void set_template_id(const int & template_id) { _template_id = template_id; }

  private: // disabled:
    Codec(const Codec & copy) :presence_map(0), serdes(copy.serdes), cvs(0), definitions(0) {}
};

ostream & operator << (ostream & out, const Codec & codec);

#endif // BB_SERVERS_SHSE_QD_DECODER_CODEC_H
