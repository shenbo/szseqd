#ifndef BB_SERVERS_SHSE_QD_DECODER_VALUES_CURRENTVALUES_H
#define BB_SERVERS_SHSE_QD_DECODER_VALUES_CURRENTVALUES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>

#include "CValues.h"


using std::vector;



/**
 * This class represents the current state of the decoded information.
 */
class CurrentValues {
  private:
    const int _slots;
    vector<CInt32::type> i32_vals;
    vector<CInt64::type> i64_vals;
    vector<CuInt32::type> u32_vals;
    vector<CuInt64::type> u64_vals;
    vector<CString::type> strings;
    vector<bool> valid;
    vector<CDecimal *> decimals;

  public:
    CurrentValues(const int slots)
      :_slots(slots)
    {
      for (int i = 0; i < slots; ++i) {
        i64_vals.push_back(0);
        u64_vals.push_back(0);
        i32_vals.push_back(0);
        u32_vals.push_back(0);
        strings.push_back("");
        valid.push_back(false);
        decimals.push_back(0);
      }
    }


    virtual ~CurrentValues() {
      i32_vals.clear();
      u32_vals.clear();
      i64_vals.clear();
      u64_vals.clear();
      strings.clear();
      valid.clear();
      for (int i = 0; i < _slots; ++i) {
        if (decimals[i]) delete decimals[i];
        decimals[i] = 0;
      }
      decimals.clear();
    }


    vector<CInt32::type> & get_array_i32() { return i32_vals; }
    vector<CInt64::type> & get_array_i64() { return i64_vals; }
    vector<CuInt32::type> & get_array_u32() { return u32_vals; }
    vector<CuInt64::type> & get_array_u64() { return u64_vals; }
    vector<CString::type> & get_array_strings() { return strings; }
    vector<bool> & get_array_valid() { return valid; }
    vector<CDecimal *> & get_array_decimals() { return decimals; }


    ostream & insert(ostream & out) const {
      out << "CurrentValues[slots=" << _slots << "]" << endl;
      out << "   valid=[" << valid[0];
      for (unsigned int i = 1; i < valid.size(); ++i) out << " " << valid[i];
      out << "]" << endl;

      out << "     i32=[" << i32_vals[0];
      for (unsigned int i = 1; i < i32_vals.size(); ++i) out << " " << i32_vals[i];
      out << "]" << endl;

      out << "     u32=[" << u32_vals[0];
      for (unsigned int i = 1; i < u32_vals.size(); ++i) out << " " << u32_vals[i];
      out << "]" << endl;

      out << "     i64=[" << i64_vals[0];
      for (unsigned int i = 1; i < i64_vals.size(); ++i) out << " " << i64_vals[i];
      out << "]" << endl;

      out << "     u64=[" << u64_vals[0];
      for (unsigned int i = 1; i < u64_vals.size(); ++i) out << " " << u64_vals[i];
      out << "]" << endl;

      out << "  string=['" << strings[0] << "'";
      for (unsigned int i = 1; i < strings.size(); ++i) out << " '" << strings[i] << "'";
      out << "]" << endl;

      out << " decimal=[" << decimals[0];
      for (unsigned int i = 1; i < decimals.size(); ++i) out << " " << decimals[i];
      out << "]" << endl;

      return out;
    }


    bool is_valid(const int slot) const {
      assert(slot < (int) valid.size());
      return valid[slot];
    }


    void reset_valid(const int slot) {
      assert(slot < (int) valid.size());
      valid[slot] = false;
    }

    void set_valid(const int slot, const bool state) {
      assert(slot < (int) valid.size());
      valid[slot] = state;
    }

    CString::type get_string(const int slot) const {
      assert(slot < (int) strings.size());
      return strings[slot];
    }


    void set_string(const int slot, const CString::type & value) {
      assert(slot < (int) strings.size());
      strings[slot] = value;
      valid[slot] = true;
    }


    CuInt32::type get_u32(const int slot) const {
      assert(slot < (int) u32_vals.size());
      return u32_vals[slot];
    }


    CuInt32::type get(const int slot, const CuInt32 & ignored) const {
      assert(slot < (int) u32_vals.size());
      return u32_vals[slot];
    }


    void set_u32(const int slot, const CuInt32::type value) {
      assert(slot < (int) u32_vals.size());
      u32_vals[slot] = value;
      valid[slot] = true;
    }


    CInt32::type get_i32(const int slot) const {
      assert(slot < (int) i32_vals.size());
      return i32_vals[slot];
    }


    CInt32::type get(const int slot, const CInt32 & ignored) const {
      assert(slot < (int) i32_vals.size());
      return i32_vals[slot];
    }


    void set_i32(const int slot, const CInt32::type value) {
      assert(slot < (int) i32_vals.size());
      i32_vals[slot] = value;
      valid[slot] = true;
    }


    CuInt64::type get_u64(const int slot) const {
      assert(slot < (int) u64_vals.size());
      return u64_vals[slot];
    }


    CuInt64::type get(const int slot, const CuInt64 & ignored) const {
      assert(slot < (int) u64_vals.size());
      return u64_vals[slot];
    }


    void set_u64(const int slot, const CuInt64::type value) {
      assert(slot < (int) u64_vals.size());
      u64_vals[slot] = value;
      valid[slot] = true;
    }


    CInt64::type get_i64(const int slot) const {
      assert(slot < (int) i64_vals.size());
      return i64_vals[slot];
    }


    CInt64::type get(const int slot, const CInt64 & ignored) const {
      assert(slot < (int) i64_vals.size());
      return i64_vals[slot];
    }


    void set_i64(const int slot, const CInt64::type value) {
      assert(slot < (int) i64_vals.size());
      i64_vals[slot] = value;
      valid[slot] = true;
    }


    CDecimal * get_decimal(const int slot) const {
      assert(slot < (int) decimals.size());
      return decimals[slot];
    }


    void set_decimal(const int slot, CDecimal * value) {
      assert(slot < (int) decimals.size());
      if (!decimals[slot]) decimals[slot] = new CDecimal();
      decimals[slot] -> set_exponent(value -> get_exponent());
      decimals[slot] -> set_mantissa(value -> get_mantissa());
      valid[slot] = true;
    }


    void reset() {
      for (int i = 0; i < _slots; ++i) valid[i] = false;
    }

  private:
    /**
     * Disabled.
     */
    CurrentValues(const CurrentValues & other) :_slots(0) {}
};


ostream & operator<<(ostream & out, const CurrentValues & cvs);


#endif // BB_SERVERS_SHSE_QD_DECODER_VALUES_CURRENTVALUES_H
