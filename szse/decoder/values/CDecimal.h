#ifndef BB_SERVERS_SHSE_QD_DECODER_VALUES_CDECIMAL_H
#define BB_SERVERS_SHSE_QD_DECODER_VALUES_CDECIMAL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <sstream>

#include "CValue.h"


using std::string;


/**
 * This class represents a floating point value in the form of a mantissa and
 * an exponent.
 */
class CDecimal : public CValue<string> {
  public:
    typedef int64_t mType;
    typedef int32_t eType;

  private:
    mType _mantissa;
    eType _exponent;

    /**
     * Disabled.
     */
    CDecimal(const CDecimal & copy) {}

  public:
    /**
     * Parses the argument string and sets the values of the mantissa and exponent.
     */
    CDecimal(const string & value) :_mantissa(0), _exponent(0) {
      set_value(value);
    }


    /**
     * Creates a CDecimal initialized to zero.
     */
    CDecimal() :_mantissa(0), _exponent(0) {
      // nothing to do!
    }


    virtual ~CDecimal() {
      // nothing to do!
    }


    /**
     * @return the exponent value of this CDecimal.
     */
    eType get_exponent() const {
      return _exponent;
    }


    /**
     * @param exponent the value to use as the exponent in this CDecimal.
     */
    void set_exponent(const eType & exponent) {
      _null = false;
      _exponent = exponent;
    }


    /**
     * @return the mantissa value of this CDecimal value.
     */
    mType get_mantissa() const {
      return _mantissa;
    }


    /**
     * @param mantissa the value to use as the mantissa of this CDecimal.
     */
    void set_mantissa(const mType & mantissa) {
      _null = false;
      _mantissa = mantissa;
    }


    /**
     * @return a string representing the floating point value of this CDecimal.
     */
    string get_decimal() const;


    /**
     * Parses the argument string and sets the mantissa and exponent values
     * accordingly
     *
     * @param value a string representing a CDecimal value, eg: '3.14'.
     */
    virtual void set_value(const string & value);


    /**
     * @return a string representing the value of this CDecimal.
     */
    virtual string get_value() const {
      return get_decimal();
    }


    /**
     * Inserts this CDecimal into the argument ostream.
     *
     * @param out the ostream to insert into.
     */
    virtual ostream & insert(ostream & out) const {
      out << "CValue<string>";
      CValue<string>::insert(out);
      out << ",CDecimal[exponent=" << _exponent << ",mantissa=" << _mantissa << "]";
      return out;
    }
};


ostream & operator << (ostream & out, const CDecimal & decimal);


#endif // BB_SERVERS_SHSE_QD_DECODER_VALUES_CDECIMAL_H
