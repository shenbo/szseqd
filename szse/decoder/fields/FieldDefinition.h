#ifndef BB_SERVERS_SHSE_QD_DECODER_FIELDS_FIELDDEFINITION_H
#define BB_SERVERS_SHSE_QD_DECODER_FIELDS_FIELDDEFINITION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <iostream>

#include "../values/CData.h"
#include <quickfix/FieldMap.h>
class Codec;

using std::string;
using std::ostream;

class FieldDefinition {
  public:
    virtual void set_current_value(Codec & codec, const string & data) = 0;
    virtual void decode(const int slot, Codec & codec, FIX::FieldMap& output) = 0;
    virtual string get_value() = 0;
    virtual CData * get_data() = 0;
    virtual void reset_data() = 0;

    /* Field Operators */
    static const int
      OPERATOR_CONSTANT = 0,
      OPERATOR_DEFAULT = 1,
      OPERATOR_COPY = 2,
      OPERATOR_INCREMENT = 3,
      OPERATOR_DELTA = 4,
      OPERATOR_TAIL = 5,
      OPERATOR_DEFAULT_PRESENCE = 6;

    /* Field Data types */
    static const int
      TYPE_U32 = 10,
      TYPE_I32 = 11,
      TYPE_U64 = 12,
      TYPE_I64 = 13,
      TYPE_STRING = 14,
      TYPE_SCALED_NUMBER = 15,
      TYPE_I64_MANTISSA = 16;

    static const char DELIMITER;

  private:
    int _field_id, _field_operator, _field_type, _template_id, _slot;
    bool _optional, _default_value, _mandatory;

  public:
    FieldDefinition(const int id, const int type, const int op, const int tid, int s, const bool nullable);

    virtual ~FieldDefinition() {
      // nothing to do!
    }

    int get_field_id() const { return _field_id; }
    int get_field_operator() const { return _field_operator; }
    int get_field_type() const { return _field_type; }
    int get_template_id() const { return _template_id; }
    int get_slot() const { return _slot; }

    void set_field_id(const int field_id) { _field_id = field_id; }
    void set_field_operator(const int field_operator) { _field_operator = field_operator; }
    void set_field_type(const int field_type) { _field_type = field_type; }
    void set_template_id(const int template_id) { _template_id = template_id; }
    void set_slot(const int slot) { _slot = slot; }

    bool is_optional() const { return _optional; }
    bool is_default_value() const { return _default_value; }
    bool is_mandatory() const { return _mandatory; }

    void set_mandatory(const bool mandatory) { _mandatory = mandatory; }
    void set_default_value(const bool default_value) { _default_value = default_value; }
    void set_optional(const bool optional) { _optional = optional; }

    string get_field_operator_name() const;
    string get_field_type_name() const;

    virtual ostream & insert(ostream & out) const;

  private:
    FieldDefinition(const FieldDefinition & other) {}
};


ostream & operator << (ostream & out, const FieldDefinition & fd);


#endif // BB_SERVERS_SHSE_QD_DECODER_FIELDS_FIELDDEFINITION_H
