// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "../FASTDecoder.h"

template<class type>
IntFieldDefinition<type>::IntFieldDefinition(const int id, const int t, 
  const int op, const int tid, const int s, const bool optional) 
    :FieldDefinition(id, t, op, tid, s, optional)
{
  _value = new type();
}


template<typename type>
void IntFieldDefinition<type>::decode(const int index, Codec & codec, FIX::FieldMap& output) {
  _value -> reset();

  if (Decoder::LOGME) {
    FLogger::get_instance().set_id(Utils::to_string(get_field_id()));
    FLogger::get_instance().set_field(get_field_type_name());
    FLogger::get_instance().set_operator(get_field_operator_name());
    FLogger::get_instance().set_optional(is_optional() ? "true" : "false");
    if (is_mandatory()) {
      FLogger::get_instance().set_pmap("M");
    } else {
      FLogger::get_instance().set_pmap(codec.get_presence_map().get_bit(index) ? "1" : "0");
    }
    CurrentValues & cvs = codec.get_current_values();
    bool valid = cvs.is_valid(get_slot());
    string current_value = valid ? Utils::to_string(cvs.get(get_slot(), *_value)) : "null";
    FLogger::get_instance().set_current(current_value);
  }

  codec.decode(index, _value);

  if (_value -> is_null()) {
    if (Decoder::LOGME) {
      FLogger::get_instance().pretty_print_display();
      FLogger::get_instance().reset();
    }
    return ;
  }
  // AF: QuickFix only exposes an IntField, which takes an int in the constructor. This is not enough for
  // the 64-bit types defined below. However, the quick fix integer converter takes a long, which is sufficient.
  // The QuickFix converter is also significantly faster than using std::stringstream.
  output.setField( get_field_id(), FIX::IntConvertor::convert( _value->get_value() ) );
}


template<class type>
string IntFieldDefinition<type>::get_value() {
  return Utils::to_string(_value -> get_value());
}


template<class type>
ostream & IntFieldDefinition<type>::insert(ostream & out) const {
  static stringstream buf;
  buf.str("");
  FieldDefinition::insert(out);
  buf << ",IntFieldDefinition[value=" << *_value << "]";
  out << buf.str();
  return out;
}


template<class type>
CData * IntFieldDefinition<type>::get_data() {
  return _value;
}


template<class type>
void IntFieldDefinition<type>::reset_data() {
  _value -> reset();
}


I32FieldDefinition::I32FieldDefinition(const int id, const int type, const int op, const int tid, const int s, const bool optional)
  :IntFieldDefinition<CInt32>(id, type, op, tid, s, optional)
{
  // nothing to do!
}


I64FieldDefinition::I64FieldDefinition(const int id, const int type, const int op, const int tid, const int s, const bool optional)
  :IntFieldDefinition<CInt64>(id, type, op, tid, s, optional)
{
  // nothing to do!
}


U32FieldDefinition::U32FieldDefinition(const int id, const int type, const int op, const int tid, const int s, const bool optional)
  :IntFieldDefinition<CuInt32>(id, type, op, tid, s, optional)
{
  // nothing to do!
}


U64FieldDefinition::U64FieldDefinition(const int id, const int type, const int op, const int tid, const int s, const bool optional)
  :IntFieldDefinition<CuInt64>(id, type, op, tid, s, optional)
{
  // nothing to do!
}


void I32FieldDefinition::set_current_value(Codec & codec, const string & data) {
  CInt32::type default_value = CInt32::parse(data);
  if (get_field_operator() == FieldDefinition::OPERATOR_INCREMENT) --default_value;
  set_default_value(true);
  codec.get_current_values().set_i32(get_slot(), default_value);
}

void U32FieldDefinition::set_current_value(Codec & codec, const string & data) {
  CuInt32::type default_value = CuInt32::parse(data);
  if (get_field_operator() == FieldDefinition::OPERATOR_INCREMENT) --default_value;
  set_default_value(true);
  codec.get_current_values().set_u32(get_slot(), default_value);
}

void I64FieldDefinition::set_current_value(Codec & codec, const string & data) {
  CInt64::type default_value = CInt64::parse(data);
  if (get_field_operator() == FieldDefinition::OPERATOR_INCREMENT) --default_value;
  set_default_value(true);
  codec.get_current_values().set_i64(get_slot(), default_value);
}

void U64FieldDefinition::set_current_value(Codec & codec, const string & data) {
  CuInt64::type default_value = CuInt64::parse(data);
  if (get_field_operator() == FieldDefinition::OPERATOR_INCREMENT) --default_value;
  set_default_value(true);
  codec.get_current_values().set_u64(get_slot(), default_value);
}
