// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "FASTDecoder.h"


string FLogger::headers[9] = {
  "ID",       "PMAP",     "Field", 
  "Operator", "Optional", "Current", 
  "Value",    "FIX",      "HEX"
};


FLogger * FLogger::instance = NULL;


bool FLogger::printed_headers = false;

