// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include <algorithm>
#include "FASTDecoder.h"

const int Codec::bit_mask[] = { 64, 32, 16, 8, 4, 2, 1, 0 };


struct DefinitionSorter {
  bool operator() (const Codec::Definition & left, const Codec::Definition & right) {
    if (left == 0 || right == 0) return -1;
    return left -> get_slot() < right -> get_slot();
  }
};


Codec::Codec(TransferEncoder & eserdes, DefinitionMap dmap, int template_id)
  : _template_id(template_id),
    presence_map(dmap.size()),
    serdes(eserdes),
    cvs(dmap.size()),
    default_values(dmap)
{
  definitions.reserve(dmap.size());
  for (DefinitionMap::iterator i = dmap.begin(), end = dmap.end(); i != end; ++i) {
    definitions.push_back(i -> first);
    i -> first -> set_current_value(*this, i -> second);
    if (i -> first -> is_optional() && (i -> second).empty()) {
      cvs.reset_valid(i -> first -> get_slot());
    }
  }
  std::sort(definitions.begin(), definitions.end(), DefinitionSorter());
}


Codec::~Codec() {
  definitions.clear();
}


Codec::Definitions & Codec::get_definitions() {
  return definitions;
}


CurrentValues & Codec::get_current_values() {
  return cvs;
}


PresenceMap & Codec::get_presence_map() {
  return presence_map;
}


CBytes * Codec::get_presence_map_off_stream() {
  static CBytes buffer = CBytes(TransferEncoder::PARSE_BUFFER_SIZE);
  serdes.parse_bytes(buffer, TransferEncoder::PARSE_BUFFER_SIZE);
  return &buffer;
}


void Codec::reset_dictionary() {
  cvs.reset();
  int count = 0;
  for (Iterator i = default_values.begin(), end = default_values.end(); i != end; ++i, ++count) {
    definitions[count] -> reset_data();
    i -> first -> set_current_value(*this, i -> second);

    if (i -> first -> is_optional() && (i -> second).empty()) {
      cvs.reset_valid(i -> first -> get_slot());
    }
  }
}


void log(const char * message) {
  static FLogger & logger = FLogger::get_instance();
  if (Decoder::LOGME) logger.append_buffer(message);
}


int Codec::parse_pmap(CBytes * buffer, const bool present, const int start, const bool first_byte) {
  int bytes = 0;
  if (present) {
    log(first_byte ? "Parsing template byte: \t" : "Parsing PMAP: \t");
        
    bytes = buffer -> get_length();

    int offset = 0;
    int idx = 0;
    CBytes::byte value = 0;
    if (bytes > 0) {
      int size = (first_byte) ? 1 : definitions.size();

      for (int i = start; i < size; ++i) {
        FieldDefinition * def = definitions.at(i);

        if (Utils::mandatory_field(def -> get_field_operator(), def -> is_optional())) {
          presence_map.get_bits()[i] = true;
          def -> set_mandatory(true);
          log("M ");
        } else {
          //Read the next byte
          if (idx == 0) {
            value = (offset < bytes) ? buffer -> get_value()[offset++] : 0; // Thanks to JM!
          }

          // See if the field is present
          presence_map.get_bits()[i] = ((value & bit_mask[idx++]) != 0);
          log(presence_map.get_bits()[i] ? "1 " :"0 ");
          
          //No more bits left in this byte
          if (idx == 7) idx = 0;
        }
      }
    }
  } else { // no presence map, everything needs to be in the input string
    log("NO PMAP Defined: ");
    for (unsigned int i = 1; i < definitions.size(); ++i) {
      presence_map.get_bits()[i] = true;
      if (Decoder::LOGME) definitions[i] -> set_mandatory(true);
      log("M ");
    }
  }
  
  if (Decoder::LOGME && first_byte) {
    FLogger::get_instance().print_buffer();
    FLogger::get_instance().reset_buffer();
  }
  return bytes;
}


template<typename type>
void Codec::decode_int(CValue<type> * cvalue, FieldDefinition * definition, const bool presence, vector<type> & values) {
  const int slot = definition -> get_slot();
  switch (definition -> get_field_operator()) {
    case FieldDefinition::OPERATOR_DEFAULT: {
      serdes.parse_int(presence, *cvalue, definition -> is_optional());
      assert(cvalue -> get_length() >= 0);
      break;
    }
    case FieldDefinition::OPERATOR_DEFAULT_PRESENCE: {
      serdes.parse_int(presence, *cvalue, definition -> is_optional());
      assert(cvalue -> get_length() >= 0);
      if (cvalue -> get_length() == 0 && definition -> is_default_value() && cvs.is_valid(slot)) {
        cvalue -> set_value(values[slot]);
      }
      break;
    }
    case FieldDefinition::OPERATOR_COPY: {
      serdes.parse_int(presence, *cvalue, definition -> is_optional());
      assert(cvalue -> get_length() >= 0);
      if (cvalue -> get_length() > 0) { // Set current value
        if (cvalue -> is_null()) {
          cvs.reset_valid(slot);
        } else {
          values[slot] = cvalue -> get_value();
          cvs.set_valid(slot, true);
        }
      } else {
        if (cvs.is_valid(slot)) {
          cvalue -> set_value(values[slot]);
          cvs.set_valid(slot, true);
        } else {
          cvalue -> set_null(true);
        }
      }
      break;
    }
    case FieldDefinition::OPERATOR_INCREMENT: {
      serdes.parse_int(presence, *cvalue, definition -> is_optional());
      assert(cvalue -> get_length() >= 0);
      if (cvalue -> get_length() > 0) { // Set current value
        if (cvalue -> is_null()) {
          cvs.reset_valid(slot);
        } else {
          values[slot] = cvalue -> get_value();
          cvs.set_valid(slot, true);
        }
      } else { // No value - use previous plus one
        if (cvs.is_valid(slot)) {
          cvalue -> set_value(values[slot] + 1);
          values[slot] = cvalue -> get_value();
          cvs.set_valid(slot, true);
        } else {
          cvalue -> set_null(true);
        }
      }
      break;
    }
    case FieldDefinition::OPERATOR_DELTA: {
      if (cvs.is_valid(slot)) {
        // Rule: delta defaults to ZERO if there is no value present
        CInt64 delta;
        serdes.parse_int(presence, delta, definition -> is_optional());
        assert(delta.get_length() >= 0);
        string currentValue = cvs.get_string(slot);
        if (delta.get_length() > 0) { // Set current value
          if (!delta.is_null()) {
            cvalue->set_value(values[slot] + delta.get_value());
            values[slot] = cvalue->get_value();
            cvs.set_valid(slot, true);
          } else {
            cvalue -> set_null(true);
          }
        } else if (delta.get_length() == 0) {
            cvalue->set_value(values[slot]);
            cvs.set_valid(slot, true);
        }
      } else {
        // Rule: A field must be present if the current value isn't valid
        serdes.parse_int(presence, *cvalue, definition -> is_optional());
        assert(cvalue -> get_length() > 0);
        if (!cvalue -> is_null()) {
          values[slot] = cvalue->get_value();
          cvs.set_valid(slot, true);
        }
      }
      break;
    }
    case FieldDefinition::OPERATOR_CONSTANT: {
      if (cvs.is_valid(slot) && presence_map.get_bit(slot)) {
        cvalue -> set_value(values[slot]);
      } else {
        assert(definition -> is_optional());
        cvalue -> set_null(true);
      }
      break;
    }
    default: {
      assert(false);
    }
  }
}


void Codec::decode_group_length(CInt32 & data) {
  serdes.parse_int(true, data, false);
  assert(data.get_length() > 0);
}


void Codec::decode(Definitions::size_type index, CInt32 * cvalue) {
  assert(index < definitions.size());
  Definition definition = definitions[index];
  decode(cvalue, definition, presence_map.get_bit(definition -> get_slot()));
}


void Codec::decode(Definitions::size_type index, CuInt32 * cvalue) {
  assert(index < definitions.size());
  Definition definition = definitions[index];
  decode(cvalue, definition, presence_map.get_bit(definition -> get_slot()));
}


void Codec::decode(Definitions::size_type index, CInt64 * cvalue) {
  assert(index < definitions.size());
  Definition definition = definitions[index];
  decode(cvalue, definition, presence_map.get_bit(definition -> get_slot()));
}


void Codec::decode(Definitions::size_type index, CuInt64 * cvalue) {
  assert(index < definitions.size());
  Definition definition = definitions[index];
  decode(cvalue, definition, presence_map.get_bit(definition -> get_slot()));
}

void Codec::decode(CInt32 * cvalue, FieldDefinition * definition, const bool presence) {
  assert(definition -> get_field_type() == FieldDefinition::TYPE_I32);
  decode_int(cvalue, definition, presence, cvs.get_array_i32());
}


void Codec::decode(CuInt32 * cvalue, FieldDefinition * definition, const bool presence) {
  assert(definition -> get_field_type() == FieldDefinition::TYPE_U32);
  decode_int(cvalue, definition, presence, cvs.get_array_u32());
}


void Codec::decode(CInt64 * cvalue, FieldDefinition * definition, const bool presence) {
  assert(definition -> get_field_type() == FieldDefinition::TYPE_I64
    ||   definition -> get_field_type() == FieldDefinition::TYPE_I64_MANTISSA);
  decode_int(cvalue, definition, presence, cvs.get_array_i64());
}


void Codec::decode(CuInt64 * cvalue, FieldDefinition * definition, const bool presence) {
  assert(definition -> get_field_type() == FieldDefinition::TYPE_U64);
  decode_int(cvalue, definition, presence, cvs.get_array_u64());
}


void Codec::decode(Definitions::size_type index, CString * data) {
  Definition definition = definitions.at(index);
  assert(definition -> get_field_type() == FieldDefinition::TYPE_STRING);
  const int slot = definition -> get_slot();
  bool process = true;
  switch (definition -> get_field_operator()) {
    case FieldDefinition::OPERATOR_DEFAULT:
      serdes.parse_string(presence_map.get_bit(slot), *data);
      assert(data -> get_length() > 0 || definition -> is_optional());
      if (definition -> is_optional()) { // optional
        string dvalue = data -> get_value();
        if (dvalue.empty() || (dvalue.length() == 1 && dvalue[0] == 0)) {
          data -> set_null(true);
        }
      }
      break;
      
    case FieldDefinition::OPERATOR_DEFAULT_PRESENCE:
      serdes.parse_string(presence_map.get_bit(slot), *data);

      if (data -> get_length() == 0 && definition -> is_default_value()) {
        data -> set_value(cvs.get_string(slot));
      } else if (definition -> is_optional()) { // optional
        string dvalue = data -> get_value();
        if (dvalue.empty() || (dvalue.length() == 1 && dvalue[0] == 0)) {
          data -> set_null(true);
        }
      }
      break;
      
    case FieldDefinition::OPERATOR_COPY:
      serdes.parse_string(presence_map.get_bit(slot), *data);
      if (data -> get_length() == 0) { // Copy current value
        if (cvs.is_valid(slot)) {
          data -> set_value(cvs.get_string(slot));
        } else {
          data -> set_null(true);
        }
      } else { // Set current value
        if (definition -> is_optional()) { // optional
          string dvalue = data -> get_value();
          if (dvalue.empty() || (dvalue.length() == 1 && dvalue[0] == 0)) {
            data -> set_null(true);
            process = false;
            cvs.reset_valid(slot);
          }
        }
        if (process) cvs.set_string(slot, data -> get_value());
      }
      break;
      
    case FieldDefinition::OPERATOR_DELTA: {
      CString delta;
      CInt32 length;
      string currentValue;
      
      if (cvs.is_valid(slot)) currentValue = cvs.get_string(slot);
      decode_group_length(length);
      
      string logv;
      string logh;
      if (Decoder::LOGME) {
        logv = FLogger::get_instance().get_value();
        logh = FLogger::get_instance().get_hex();
      }
      
      int deltaLength = length.get_value();
      if (definition -> is_optional()) { // optional
        if (deltaLength == 0) {
          data -> set_null(true);
          process = false;
        } else if (deltaLength > 0) --deltaLength;
      }
      
      if (!process) return;
      serdes.parse_string(presence_map.get_bit(slot), delta);
      string deltaValue = delta.get_value();
      string newValue;
      if (deltaValue.length() == 1 && deltaValue[0] == 0) deltaValue = "";
      
      if (Decoder::LOGME) {
        logv += deltaValue.empty() ? " null" : " " + FLogger::get_instance().get_value();
        logh += " " + FLogger::get_instance().get_hex();
        FLogger::get_instance().set_value(logv);
        FLogger::get_instance().set_hex(logh);
      }
      
      if (deltaLength == 0) {
        newValue = currentValue + deltaValue;
        data -> set_value(newValue);
        cvs.set_string(slot, newValue);
        
      } else if (deltaLength > 0) {
        newValue = currentValue.substr(0, currentValue.length() - deltaLength);
        data -> set_value(newValue + deltaValue);
        cvs.set_string(slot, newValue + deltaValue);
        
      } else {
        ++deltaLength;
        if (deltaLength == 0) { // excess -1 decoding
          newValue = deltaValue + currentValue;
          data -> set_value(newValue);
          cvs.set_string(slot, newValue);
          
        } else if (deltaLength < 0) {
          deltaLength = deltaLength * -1;
          newValue = deltaValue + currentValue.substr(deltaLength, currentValue.length() - deltaLength);
          data -> set_value(newValue);
          cvs.set_string(slot, newValue);
          
        }
      }
      break;
    }
    case FieldDefinition::OPERATOR_CONSTANT:
      if (cvs.is_valid(slot) && presence_map.get_bit(slot)) {
        data -> set_value(cvs.get_string(slot));
      } else {
        assert(definition -> is_optional());
        data -> set_null(true);
      }
      break;
      
    default:
      assert(false);
  }
}


void Codec::decode(Definitions::size_type index, CDecimal * data) {
  ScaledFieldDefinition * decimalDec = (ScaledFieldDefinition *) definitions[index];
  
  const int slot = decimalDec -> get_slot();
  string logh;
  string logv;
  switch (decimalDec -> get_field_operator()) {
    case FieldDefinition::OPERATOR_CONSTANT: {
      if (cvs.is_valid(slot) && presence_map.get_bit(slot)) {
        data -> set_exponent(cvs.get_decimal(slot) -> get_exponent());
        data -> set_mantissa(cvs.get_decimal(slot) -> get_mantissa());
      } else {
        assert(decimalDec -> is_optional());
        decimalDec -> set_null_field();
      }
      break;
    }
    case FieldDefinition::OPERATOR_COPY: {
      I32FieldDefinition * expFD = decimalDec -> get_exponent();
      expFD -> set_field_operator(FieldDefinition::OPERATOR_COPY);
      CInt32 * exponent = (CInt32 *) expFD -> get_data();
      
      decode(exponent, expFD, presence_map.get_bit(slot));
      if (Decoder::LOGME) {
        logv = FLogger::get_instance().get_value();
        logh = FLogger::get_instance().get_hex();
      }
      assert(exponent -> get_length() >= 0);
      if (exponent -> get_length() > 0) {
        if (exponent -> is_null()) {
          cvs.reset_valid(slot);
        } else {
          I64FieldDefinition * mantFD = decimalDec -> get_mantissa();
          CInt64 * mantissa = (CInt64 *) mantFD -> get_data();
          decode(mantissa, mantFD, true);
          if (Decoder::LOGME) {
            logv = logv + " "+ FLogger::get_instance().get_value();
            logh = logh + " "+ FLogger::get_instance().get_hex();
            FLogger::get_instance().set_hex(logh);
            FLogger::get_instance().set_value(logv);
          }
          
          data -> set_exponent(exponent -> get_value());
          data -> set_mantissa(mantissa -> get_value());
          data -> set_length(exponent -> get_length() + mantissa -> get_length());
          
          cvs.set_decimal(slot, data);
        }
      } else if (exponent -> get_length() == 0) { // No value - copy current value
        if (cvs.is_valid(slot)) {
          data -> set_exponent(cvs.get_decimal(slot) -> get_exponent());
          data -> set_mantissa(cvs.get_decimal(slot) -> get_mantissa());
        } else {
          decimalDec -> set_null_field();
        }
      }
      break;
    }
    case FieldDefinition::OPERATOR_DELTA: {
      I32FieldDefinition * expFDd = decimalDec -> get_exponent();
      I64FieldDefinition * mantFDd = decimalDec -> get_mantissa();

      CInt32 * exponentd = (CInt32 *) expFDd -> get_data();
      CInt64 * mantissad = (CInt64 *) mantFDd -> get_data();
      decode(exponentd, expFDd, true);
      if (Decoder::LOGME) {
        logv = FLogger::get_instance().get_value();
        logh = FLogger::get_instance().get_hex();
      }
      
      if (expFDd -> get_data() -> is_null() && (!expFDd -> is_optional() ||
          Utils::mandatory_field(expFDd -> get_field_operator(), expFDd -> is_optional()))) {
        return;
      }
      decode(mantissad, mantFDd, true);
      if (Decoder::LOGME) {
        logv = logv + " "+ FLogger::get_instance().get_value();
        logh = logh + " "+ FLogger::get_instance().get_hex();
        FLogger::get_instance().set_hex(logh);
        FLogger::get_instance().set_value(logv);
      }
      
      int32_t x = exponentd -> get_value();
      int64_t m = mantissad -> get_value();
      
      if (cvs.is_valid(slot)) {
        x += cvs.get_decimal(slot) -> get_exponent();
        m += cvs.get_decimal(slot) -> get_mantissa();
      }
      
      data -> set_exponent(x);
      data -> set_mantissa(m);
      data -> set_length(exponentd -> get_length() + mantissad -> get_length());
      
      expFDd -> get_data() -> set_null(false);
      cvs.set_i32(slot, x);
      cvs.set_i64(slot, m);
      cvs.set_decimal(slot, data);
      break;
    }
    case FieldDefinition::OPERATOR_INCREMENT: {
      assert(false);
    }
    case FieldDefinition::OPERATOR_DEFAULT_PRESENCE: {
      I32FieldDefinition * dexpFDdef = decimalDec -> get_exponent();
      CInt32 * dexponentdef = (CInt32 *) dexpFDdef -> get_data();
      
      decode(dexponentdef, dexpFDdef, presence_map.get_bit(slot));
      if (Decoder::LOGME) {
        logv = FLogger::get_instance().get_value();
        logh = FLogger::get_instance().get_hex();
      }
      
      if (dexponentdef -> get_length() == 0) {
        if (!dexpFDdef -> is_optional() || (dexpFDdef -> is_optional()
            && !dexponentdef -> is_null())) {
          data -> set_exponent(cvs.get_decimal(slot) -> get_exponent());
          data -> set_mantissa(cvs.get_decimal(slot) -> get_mantissa());
        }
        
      } else {
        if (dexponentdef -> is_null()) return;
        
        I64FieldDefinition * dmantFDdef = decimalDec -> get_mantissa();
        CInt64 * dmantissadef = (CInt64 *) dmantFDdef -> get_data();
        decode(dmantissadef, dmantFDdef, true);
        
        if (Decoder::LOGME) {
          logv = logv + " "+ FLogger::get_instance().get_value();
          logh = logh + " "+ FLogger::get_instance().get_hex();
          FLogger::get_instance().set_hex(logh);
          FLogger::get_instance().set_value(logv);
        }
        
        dexponentdef -> set_null(false);
        int64_t dmef = dmantissadef -> get_value();
        data -> set_exponent(dexponentdef -> get_value());
        data -> set_mantissa(dmef);
        data -> set_length(dexponentdef -> get_length() + dmantissadef -> get_length());
      }
      break;
    }
    default: {
      I32FieldDefinition * expFDdef = decimalDec -> get_exponent();
      CInt32 * exponentdef = (CInt32 *) expFDdef -> get_data();
      
      decode(exponentdef, expFDdef, true);
      if (Decoder::LOGME) {
        logv = FLogger::get_instance().get_value();
        logh = FLogger::get_instance().get_hex();
      }
      
      if (exponentdef -> is_null()) return;
      
      I64FieldDefinition * mantFDdef = decimalDec -> get_mantissa();
      CInt64 * mantissadef = (CInt64 *) mantFDdef -> get_data();
      decode(mantissadef, mantFDdef, true);
      
      if (Decoder::LOGME) {
        logv = logv + " "+ FLogger::get_instance().get_value();
        logh = logh + " "+ FLogger::get_instance().get_hex();
        FLogger::get_instance().set_hex(logh);
        FLogger::get_instance().set_value(logv);
      }
      
      int64_t mef = mantissadef -> get_value();
      int32_t xef = exponentdef -> get_value();
      data -> set_exponent(xef);
      data -> set_mantissa(mef);
      data -> set_length(exponentdef -> get_length() + mantissadef -> get_length());
      
      exponentdef -> set_null(false);
      cvs.set_i32(slot, xef);
      cvs.set_i64(slot, mef);
      cvs.set_decimal(slot, data);
      break;
    }
  }
}


ostream & operator << (ostream & out, const Codec & codec) {
  out << "Codec[template_id=" << codec.get_template_id();
  out << "]";
  return out;
}
