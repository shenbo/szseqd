#ifndef BB_SERVERS_SHSE_QD_DECODER_FLOGGER_H
#define BB_SERVERS_SHSE_QD_DECODER_FLOGGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstdio>
#include <sstream>
#include <iostream>

using std::stringstream;
using std::string;
using std::cout;
using std::endl;


class FLogger {
  private:
    stringstream buffer;
    static string headers[9];
    static FLogger * instance;
    static bool printed_headers;

    static void print(const string * array) {
      printf("%5s %5s %8s %13s %9s %20s %20s %25s",
        array[0].c_str(), array[1].c_str(), array[2].c_str(),
        array[3].c_str(), array[4].c_str(), array[5].c_str(),
        array[6].c_str(), array[7].c_str());
      cout << "     " << array[8] << endl;
    }

  public:
    static FLogger& get_instance() {
      if (instance == NULL) {
        instance = new FLogger();
      }
      return *instance;
    }

  private:
    string values[9];
    FLogger() {
      // nothing to do!
    }

  public:
    virtual ~FLogger() {
      // nothing to do!
    }

    void print_header() const {
      print(headers);
      printed_headers = true;
    }

    void pretty_print_display() const {
      if (printed_headers) print(values);
    }

    void append_buffer(const char * value) {
      buffer << value;
    }

    void reset_buffer() {
      buffer.str("");
    }

    void print_buffer() const {
      cout << buffer.str() << endl;
    }

    void println() const {
      cout << endl;
    }

    void reset() {
      if (printed_headers) for (int i = 0; i < 9; ++i) values[i] = "";
    }

    string   get_hex() const { return values[8]; }
    string get_value() const { return values[6]; }

    void set_id      (const string & value) { values[0] = value; }
    void set_pmap    (const string & value) { values[1] = value; }
    void set_field   (const string & value) { values[2] = value; }
    void set_operator(const string & value) { values[3] = value; }
    void set_optional(const string & value) { values[4] = value; }
    void set_current (const string & value) { values[5] = value; }
    void set_value   (const string & value) { values[6] = value; }
    void set_fix     (const string & value) { values[7] = value; }
    void set_hex     (const string & value) { values[8] = value; }
};


#endif // BB_SERVERS_SHSE_QD_DECODER_FLOGGER_H
