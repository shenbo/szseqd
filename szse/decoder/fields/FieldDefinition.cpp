// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "../FASTDecoder.h"


const char FieldDefinition::DELIMITER = 0x01;


FieldDefinition::FieldDefinition(const int id, const int type, const int op, 
  const int tid, const int s, const bool nullable) 
{
  _field_id = id;
  _field_type = type;
  _field_operator = op;
  _template_id = tid;
  _slot = s;
  _optional = nullable;
}


string FieldDefinition::get_field_operator_name() const {
  switch (_field_operator) {
    case (OPERATOR_CONSTANT): return "Constant";
    case (OPERATOR_DEFAULT): return "none";
    case (OPERATOR_DEFAULT_PRESENCE): return "Default";
    case (OPERATOR_COPY): return "Copy";
    case (OPERATOR_INCREMENT): return "Increment";
    case (OPERATOR_DELTA): return "Delta";
    case (OPERATOR_TAIL): return "Tail";
    default: return "Unknown";
  }
}


string FieldDefinition::get_field_type_name() const {
  switch(_field_type) {
    case (TYPE_U32): return "U32";
    case (TYPE_I32): return "I32";
    case (TYPE_U64): return "U64";
    case (TYPE_I64): case (TYPE_I64_MANTISSA): return "I64";
    case (TYPE_STRING): return "String";
    case (TYPE_SCALED_NUMBER): return "Decimal";
    default: return "Unknown";
  }
}


ostream & FieldDefinition::insert(ostream & out) const {
  out << "FieldDefinition[" 
      << "id=" << get_field_id() << "," 
      << "operator=" << get_field_operator_name() << ","
      << "type=" << get_field_type_name() << ","
      << "template_id=" << get_template_id() << ","
      << (is_optional() ? "optional," : "")
      << (is_default_value() ? "default_value," : "")
      << "slot=" << get_slot()
      << "]";
  return out;
}


ostream & operator << (ostream & out, const FieldDefinition & fd) {
  return fd.insert(out);
}

