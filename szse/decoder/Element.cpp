// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "Element.h"

#include <stdexcept>

#define TIXML_USE_STL
#include <tinyxml.h>


Element * Element::get_root(const std::string & template_filename)
{
    TiXmlDocument doc(template_filename);

    if (!doc.LoadFile())
        throw std::runtime_error("failed to load file: " + template_filename);

    return new Element(doc.RootElement());
}


Element::Element(TiXmlElement * element, Element * parent)
{
    assert(element != NULL);

    this->parent = parent;
    name = element->Value();

    for (TiXmlAttribute* attrib = element->FirstAttribute(); attrib != NULL; attrib = attrib->Next())
        attributes.insert(std::make_pair(attrib->Name(), attrib->Value()));

    for (TiXmlElement* child = element->FirstChildElement(); child != NULL; child = child->NextSiblingElement())
        children.push_back(new Element(child, this));
}


Element::~Element()
{
    for (Iterator i = children.begin(), e = children.end(); i != e; ++i) 
        delete *i;
}
