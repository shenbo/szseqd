#ifndef BB_SERVERS_SHSE_QD_DECODER_FIELDS_SCALEDEXPONENTFIELDDEFINITION_H
#define BB_SERVERS_SHSE_QD_DECODER_FIELDS_SCALEDEXPONENTFIELDDEFINITION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include "IntFieldDefinition.h"

class ScaledExponentFieldDefinition : public I32FieldDefinition {
  public:
    ScaledExponentFieldDefinition(const int id, const int type, const int op,
      const int tid, const int s, const bool optional);
    virtual ~ScaledExponentFieldDefinition() {}

    virtual void decode(const int slot, Codec & codec, FIX::FieldMap& output);
};

#endif // BB_SERVERS_SHSE_QD_DECODER_FIELDS_SCALEDEXPONENTFIELDDEFINITION_H
