#ifndef BB_SERVERS_SHSE_QD_DECODER_FIELDS_GROUPFIELDDEFINITION_H
#define BB_SERVERS_SHSE_QD_DECODER_FIELDS_GROUPFIELDDEFINITION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include "FieldDefinition.h"
#include "../values/CValues.h"
#include "../Codec.h"

class GroupFieldDefinition : public FieldDefinition {
  private:
    bool presence_map;
    Codec & group_codec;
    CuInt32 group_length;
    string parsed_value;

  public:
    GroupFieldDefinition(int id, int type, int op, int tid, int s, Codec & codec, bool optional);
    virtual ~GroupFieldDefinition() {}

    virtual void set_current_value(Codec & codec, const string & data) {
      CuInt32::type default_value = CuInt32::parse(data);
      if (get_field_operator() == FieldDefinition::OPERATOR_INCREMENT) --default_value;
      set_default_value(true);
      codec.get_current_values().set_u32(get_slot(), default_value);
    }

    virtual void reset_data() { group_codec.reset_dictionary(); }
    virtual CData * get_data() { return 0; }
    virtual string get_value() { return parsed_value; }
    virtual void decode(const int slot, Codec & codec, FIX::FieldMap& output);

    bool has_presence_map() const { return presence_map; }
    void set_presence_map(const bool value) { presence_map = value; }
    Codec & get_group_codec() { return group_codec; }
};

#endif // BB_SERVERS_SHSE_QD_DECODER_FIELDS_GROUPFIELDDEFINITION_H
