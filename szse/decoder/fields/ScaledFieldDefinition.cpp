// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include "../FASTDecoder.h"


ScaledFieldDefinition::ScaledFieldDefinition(const int id, const int type, 
  const int op, const int tid, const int s, const Exponent & exponent, 
  const Mantissa & mantissa, const bool optional)
    : FieldDefinition(id, type, op, tid, s, optional), 
      _exponent(exponent),
      _mantissa(mantissa)
{
  _value = new CDecimal();
  _value -> set_exponent(exponent -> get_int());
  _value -> set_mantissa(mantissa -> get_int());
}


void ScaledFieldDefinition::set_current_value(Codec & codec, const string & data) {
  delete _value;
  _value = new CDecimal(data);
  set_default_value(true);
  codec.get_current_values().set_decimal(get_slot(), _value);
  _exponent -> set_current_value(codec, Utils::to_string(_value -> get_exponent()));
  _mantissa -> set_current_value(codec, Utils::to_string(_value -> get_mantissa()));
  ((CInt32 *) _exponent -> get_data()) -> set_value(_value -> get_exponent());
  ((CInt64 *) _mantissa -> get_data()) -> set_value(_value -> get_mantissa());
}


void ScaledFieldDefinition::decode(const int slot, Codec & codec, FIX::FieldMap& output) {
  if (Decoder::LOGME) {
    FLogger::get_instance().set_id(Utils::to_string(get_field_id()));
    FLogger::get_instance().set_field(get_field_type_name());
    FLogger::get_instance().set_operator(get_field_operator_name());
    FLogger::get_instance().set_optional(is_optional() ? "true" : "false");
    if (is_mandatory()) {
      FLogger::get_instance().set_pmap("M");
    } else {
      FLogger::get_instance().set_pmap((codec.get_presence_map().get_bit(slot)) ? "1" : "0");
    }
    const int slot = get_slot();
    CurrentValues & cvs = codec.get_current_values();
    string current = cvs.is_valid(slot) ? Utils::to_string(cvs.get_decimal(slot) -> get_decimal()) : "null";
    FLogger::get_instance().set_current(current);
  }

  codec.decode(slot, _value);
  if (_exponent -> get_data() -> is_null()) {
    if (Decoder::LOGME) {
      FLogger::get_instance().pretty_print_display();
      FLogger::get_instance().reset();
    }
    return;
  }
  output.setField( get_field_id(), get_value() );
}


string ScaledFieldDefinition::get_value() {
  return _value -> get_decimal();
}


CDecimal * ScaledFieldDefinition::get_data() {
  return _value;
}


void ScaledFieldDefinition::reset_data() {
  _value -> reset();
}


void ScaledFieldDefinition::set_null_field() {
  _exponent -> get_data() -> set_null(true);
}
